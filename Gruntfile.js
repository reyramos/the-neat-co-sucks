// Generated on 2014-05-03 using generator-angular 0.8.0
'use strict';


module.exports = function(grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt, {
        config: 'package.json',
        scope: ['devDependencies', 'dependencies']
    });

    var appConfig = {
        app: require('./bower.json').appPath || 'app',
        dist: 'dist'
    };


    // Define the configuration for all the tasks
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // Project settings
        yeoman: appConfig,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            options: {
                livereload: true,
                spawn: false
            },
            jsTest: {
                files: ['test/spec/**/*.js'],
                tasks: ['newer:jshint:test', 'karma']
            },
            configFiles: {
                files: ['Gruntfile.js', 'config/*.js'],
                options: {
                    reload: true
                }
            },
            less: {
                files: [
                    "{.tmp,<%= yeoman.app %>}/css/**/*.less",
                    "{.tmp,<%= yeoman.app %>}/**/*.less"
                ],
                options: {
                    livereload: true,
                    spawn: false
                }
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    //'<%= yeoman.app %>/{,*/}*.html',
                    //'<%= yeoman.app %>/*.template',
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        //Compile less files to css
        less: {
            dev: {
                options: {
                    paths: ["<%= yeoman.app %>"]
                },
                files: {
                    ".tmp/styles.css": "<%= yeoman.app %>/styles.less"
                }
            },
            dist: {
                options: {
                    compress: true
                },
                files: {
                    ".tmp/styles.css": "<%= yeoman.app %>/styles.less"
                }
            }
        },
        concat: {
            dist: {
                src: ['<%= yeoman.app %>/js/vendor/socket.io-1.3.2.js','.tmp/scripts-require.js', '<%= yeoman.app %>/js/bootstrap.js'],
                dest: ".tmp/scripts-concat.js"
            }
        },
        requirejs: {
            dist: {
                options: {
                    baseUrl: "<%= yeoman.app %>/js",
                    findNestedDependencies: true,
                    logLevel: 0,
                    mainConfigFile: "<%= yeoman.app %>/js/main.js",
                    name: 'main',
                    include: ['../../.tmp/views.js'],
                    onBuildWrite: function(moduleName, path, contents) {
                        var modulesToExclude, shouldExcludeModule;
                        modulesToExclude = ['main', 'bootstrap'];
                        shouldExcludeModule = modulesToExclude.indexOf(moduleName) >= 0;
                        if (shouldExcludeModule) {
                            return '';
                        }
                        return contents;
                    },
                    optimize: "none",
                    out: ".tmp/scripts-require.js",
                    preserveLicenseComments: false,
                    skipModuleInsertion: true,
                    generateSourceMaps: true
                }
            }
        },
        template: {
            dev: {
                files: {
                    ".tmp/index.html": "<%= yeoman.app %>/index.template"
                },
                environment: "dev"
            },
            dist: {
                files: {
                    ".tmp/index-concat.html": "<%= yeoman.app %>/index.template"
                },
                environment: "dist",
                css_sources: '<%= grunt.file.read(".tmp/styles.css") %>',
                js_sources: '<%= grunt.file.read(".tmp/scripts.js") %>'
            }


        },
        //minify Angular Js, html files with $templateCache
        ngtemplates: {
            app: {
                options: {
                    prefix: '/',
                    htmlmin: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeAttributeQuotes: true,
                        removeComments: true,
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true
                    }
                },
                cwd: '<%= yeoman.app %>',
                src: ["views/**/*.html"],
                dest: '.tmp/views.js'
            }
        },
        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                hostname: "0.0.0.0",
                livereload: 35729

            },
            livereload: {
                options: {
                    middleware: function(connect) {
                        return [
                            connect.static('.tmp'),
                            connect().use(
                                '/lib',
                                connect.static('./lib')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            test: {
                options: {
                    port: 9001,
                    base: [
                        '.tmp',
                        'test',
                        '<%= yeoman.app %>'
                    ]
                }
            },
            dist: {
                options: {
                    base: '<%= yeoman.dist %>'
                }
            }
        },
        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            test: {
                options: {
                    jshintrc: 'test/.jshintrc'
                },
                src: ['test/spec/**/*.js']
            }
        },
        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.dist %>/*',
                        '!<%= yeoman.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },
        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp',
                    src: '**/*.css',
                    dest: '.tmp'
                }]
            }
        },
        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true,
                    removeAttributeQuotes: true,
                    removeComments: true, // Only if you don't use comment directives!
                    removeEmptyAttributes: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true
                },
                files: {
                    ".tmp/index.html": ".tmp/index-concat.html"
                }
            }
        },

        // Copies remaining files to places other tasks can use, do not override the index.html file
        copy: {
            app: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>',
                    src: [
                        '*.{ico,png,txt}', '.htaccess', 'img/**/*.{gif,jpeg,jpg,png}', 'fonts/*.{png,jpg,jpeg,gif,webp,svg,eot,ttf,woff}', 'configs/**/*.json', 'sound/**/*.{mp3,ogg,wave,mp4}'
                    ]
                }, {
                    expand: true,
                    cwd: '.tmp/images',
                    dest: '<%= yeoman.dist %>/img',
                    src: ['generated/*']
                }]
            },
            temp: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: ".tmp",
                    dest: "<%= yeoman.dist %>",
                    src: [
                        'img/**/*.{gif,jpeg,jpg,png}', 'index.html'
                    ]
                }]
            },
            styles: {
                expand: true,
                cwd: '<%= yeoman.app %>/css',
                dest: '.tmp/css/',
                src: '**/*.css'
            }
        },
        // Run some tasks in parallel to speed up the build process
        concurrent: {
            server: [
                'copy:styles'
            ],
            test: [
                'copy:styles'
            ],
            dist: [
                'copy:temp'
            ],
            options: {
                limit: 5,
                logConcurrentOutput: true
            }
        },
        uglify: {
            options: {
                mangle: false,
                compress: true,
                preserveComments: false,
                sourceMap: true,
                sourceMapName: "<%= yeoman.dist %>/scripts.js.map",
                sourceMapIncludeSources: true,
                sourceMapIn: '.tmp/scripts-require.js.map',
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
            },
            dist: {
                options: {
                    mangle: false,
                    compress: {
                        drop_console: true
                    }
                },
                files: {
                    '.tmp/scripts.js': '.tmp/scripts-concat.js'
                }
            }
        },
        env: {
            dev: {
                NODE_ENV: 'development',
                DEST: 'temp'
            },
            build: {
                NODE_ENV: 'production',
                DEST: 'dist'
            }
        },
        compress: {
            dist: {
                options: {
                    mode: 'gzip',
                    pretty: true
                },
                src: ['dist/index.html'],
                dest: 'dist/index.html',
                ext: '.html'
            }
        },
        // Test settings
        karma: {
            unit: {
                configFile: 'karma.conf.js',
                singleRun: true
            }
        }
    });

    grunt.registerTask("socket.io", function() {
        return require("./socket/server.js");
    });

    grunt.registerTask('serve', function(target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }
        grunt.task.run([
            'build',
            'env:dev',
            'socket.io',
            'template:dev',
            'concurrent:server',
            'autoprefixer',
            'connect:livereload',
            'watch'
        ]);
    });
    grunt.registerTask('test', [
        'clean:server',
        'concurrent:test',
        'autoprefixer',
        'connect:test',
        'karma'
    ]);

    grunt.registerTask('build', [
        'clean:dist' //clean directory
        , 'ngtemplates' //minify Angular Js, html files in templateCache
        , 'requirejs' //get all dependencies and combine in one file
        , 'concat' //concat all js files
        , 'uglify' //uglify all js files
        , 'less:dist' //compile less files in .tmp
        , 'autoprefixer' //add all vendor prefixes to styles
        , 'template:dist' //concat all the compile files into index.html
        , 'htmlmin' //clean html
        , 'concurrent:dist' //performs some task paraller to others
        , 'copy:app' //copy the remaining app files to dist
        //, "compress" //compress the index.html files

    ]);
    grunt.registerTask('default', [
        'newer:jshint',
        'test',
        'build'

    ]);
};
