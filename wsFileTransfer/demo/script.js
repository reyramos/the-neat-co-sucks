/**
 * Created by redroger on 10/11/14.
 */

(function( $ )
{
	var host = "ws://" + window.location.host + ":8999", socket;
	try{
		socket = new WebSocket ( host );
		log ( 'WebSocket - status ' + socket.readyState );
		socket.onopen = function( msg ) { log ( "Welcome - status " + this.readyState ); };
		socket.onmessage = function( msg )
		{
			console.log ( 'onmessage', msg );
			log ( msg.data );
		};
		socket.onclose = function( msg ) { log ( "Disconnected - status " + this.readyState ); };
	}catch( ex ){
		log ( ex );
	}
	$ ( "#msg" ).focus ().keyup (
		function( event )
		{
			if( event.keyCode == 13 ){
				send ();
			}
		}
	);
	$ ( "#send" ).on ( 'click', send )
	$ ( "#quit" ).on ( 'click', quit )
	function byteCount ( s )
		{
			return encodeURI ( s ).split ( /%..|./ ).length - 1;
		}

	function send ()
		{
			var msg = $ ( '#msg' ).val ();
			try{
				socket.send ( msg );
				log ( 'Sent (' + byteCount ( msg ) + " bytes)" );
			}catch( ex ){
				log ( ex );
			}
		}

	function quit ()
		{
			log ( "Goodbye!" );
			socket.close ();
			socket = null;
		}

	// Utilities
	function log ( msg ) { document.getElementById ( "log" ).innerHTML += msg + "<br>"; }

	/*****************************************************************************************
	 UPLOAD FUNCTIONALITY
	 *****************************************************************************************/

	function handleFileSelect ( evt )
		{
			var files = evt.target.files; // FileList object
			for( var i = 0, f; f = files[i]; i++ ){
				!function outer ( i )
				{
					uploadFile ( files[i] )
				} ( i );
			}
		}

	function uploadFile ( files )
		{
			var $transfer = document.createElement ( "div" );
			$transfer.className = "transfer";
			var $progress = document.createElement ( "div" );
			$progress.className = "progress";
			var $progressBar = document.createElement ( "div" );
			$progressBar.className = "progressBar";
			$progressBar.appendChild ( $progress );
			$transfer.appendChild ( $progressBar );
			document.getElementById ( 'progresses' ).appendChild ( $transfer );

			var transfer = new wsFileTransfer (
				{
					url:host,
					file:files,
					blockSize:1024,
					progress:function( data )
				{
					$progress.style.width = data.progress + '%';
				}, success:function( event )
				{
					$progress.className += " finished" ;
				}
				}
			);
		}

	// Check for the various File API support.
	if( window.File && window.FileReader && window.FileList && window.Blob ){
		// Upload the files when user submit the form
		document.getElementById ( 'files' ).addEventListener ( 'change', handleFileSelect, false );
	}else{
		console.warn ( 'The File APIs are not fully supported in this browser.' );
	}
	//To use the upload button
	//var files = document.getElementById('files').files;
	//if (!files.length) {
	//	alert('Please select a file!');
	//	return;
	//}
	//
	//var file = files[0];
}) ( jQuery )


