<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Datamatrix' => $vendorDir . '/tecnick.com/tcpdf/include/barcodes/datamatrix.php',
    'FPDF' => $vendorDir . '/setasign/fpdi/fpdi2tcpdf_bridge.php',
    'FPDF_TPL' => $vendorDir . '/setasign/fpdi/fpdf_tpl.php',
    'FPDI' => $vendorDir . '/setasign/fpdi/fpdi.php',
    'FilterASCII85' => $vendorDir . '/setasign/fpdi/filters/FilterASCII85.php',
    'FilterASCII85_FPDI' => $vendorDir . '/setasign/fpdi/filters/FilterASCII85_FPDI.php',
    'FilterLZW' => $vendorDir . '/setasign/fpdi/filters/FilterLZW.php',
    'FilterLZW_FPDI' => $vendorDir . '/setasign/fpdi/filters/FilterLZW_FPDI.php',
    'Logger' => $vendorDir . '/apache/log4php/src/main/php/Logger.php',
    'LoggerAppender' => $vendorDir . '/apache/log4php/src/main/php/LoggerAppender.php',
    'LoggerAppenderConsole' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderConsole.php',
    'LoggerAppenderDailyFile' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderDailyFile.php',
    'LoggerAppenderEcho' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderEcho.php',
    'LoggerAppenderFile' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderFile.php',
    'LoggerAppenderFirePHP' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderFirePHP.php',
    'LoggerAppenderMail' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderMail.php',
    'LoggerAppenderMailEvent' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderMailEvent.php',
    'LoggerAppenderMongoDB' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderMongoDB.php',
    'LoggerAppenderNull' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderNull.php',
    'LoggerAppenderPDO' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderPDO.php',
    'LoggerAppenderPhp' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderPhp.php',
    'LoggerAppenderPool' => $vendorDir . '/apache/log4php/src/main/php/LoggerAppenderPool.php',
    'LoggerAppenderRollingFile' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderRollingFile.php',
    'LoggerAppenderSocket' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderSocket.php',
    'LoggerAppenderSyslog' => $vendorDir . '/apache/log4php/src/main/php/appenders/LoggerAppenderSyslog.php',
    'LoggerAutoloader' => $vendorDir . '/apache/log4php/src/main/php/LoggerAutoloader.php',
    'LoggerConfigurable' => $vendorDir . '/apache/log4php/src/main/php/LoggerConfigurable.php',
    'LoggerConfigurationAdapter' => $vendorDir . '/apache/log4php/src/main/php/configurators/LoggerConfigurationAdapter.php',
    'LoggerConfigurationAdapterINI' => $vendorDir . '/apache/log4php/src/main/php/configurators/LoggerConfigurationAdapterINI.php',
    'LoggerConfigurationAdapterPHP' => $vendorDir . '/apache/log4php/src/main/php/configurators/LoggerConfigurationAdapterPHP.php',
    'LoggerConfigurationAdapterXML' => $vendorDir . '/apache/log4php/src/main/php/configurators/LoggerConfigurationAdapterXML.php',
    'LoggerConfigurator' => $vendorDir . '/apache/log4php/src/main/php/LoggerConfigurator.php',
    'LoggerConfiguratorDefault' => $vendorDir . '/apache/log4php/src/main/php/configurators/LoggerConfiguratorDefault.php',
    'LoggerException' => $vendorDir . '/apache/log4php/src/main/php/LoggerException.php',
    'LoggerFilter' => $vendorDir . '/apache/log4php/src/main/php/LoggerFilter.php',
    'LoggerFilterDenyAll' => $vendorDir . '/apache/log4php/src/main/php/filters/LoggerFilterDenyAll.php',
    'LoggerFilterLevelMatch' => $vendorDir . '/apache/log4php/src/main/php/filters/LoggerFilterLevelMatch.php',
    'LoggerFilterLevelRange' => $vendorDir . '/apache/log4php/src/main/php/filters/LoggerFilterLevelRange.php',
    'LoggerFilterStringMatch' => $vendorDir . '/apache/log4php/src/main/php/filters/LoggerFilterStringMatch.php',
    'LoggerFormattingInfo' => $vendorDir . '/apache/log4php/src/main/php/helpers/LoggerFormattingInfo.php',
    'LoggerHierarchy' => $vendorDir . '/apache/log4php/src/main/php/LoggerHierarchy.php',
    'LoggerLayout' => $vendorDir . '/apache/log4php/src/main/php/LoggerLayout.php',
    'LoggerLayoutHtml' => $vendorDir . '/apache/log4php/src/main/php/layouts/LoggerLayoutHtml.php',
    'LoggerLayoutPattern' => $vendorDir . '/apache/log4php/src/main/php/layouts/LoggerLayoutPattern.php',
    'LoggerLayoutSerialized' => $vendorDir . '/apache/log4php/src/main/php/layouts/LoggerLayoutSerialized.php',
    'LoggerLayoutSimple' => $vendorDir . '/apache/log4php/src/main/php/layouts/LoggerLayoutSimple.php',
    'LoggerLayoutTTCC' => $vendorDir . '/apache/log4php/src/main/php/layouts/LoggerLayoutTTCC.php',
    'LoggerLayoutXml' => $vendorDir . '/apache/log4php/src/main/php/layouts/LoggerLayoutXml.php',
    'LoggerLevel' => $vendorDir . '/apache/log4php/src/main/php/LoggerLevel.php',
    'LoggerLocationInfo' => $vendorDir . '/apache/log4php/src/main/php/LoggerLocationInfo.php',
    'LoggerLoggingEvent' => $vendorDir . '/apache/log4php/src/main/php/LoggerLoggingEvent.php',
    'LoggerMDC' => $vendorDir . '/apache/log4php/src/main/php/LoggerMDC.php',
    'LoggerNDC' => $vendorDir . '/apache/log4php/src/main/php/LoggerNDC.php',
    'LoggerOptionConverter' => $vendorDir . '/apache/log4php/src/main/php/helpers/LoggerOptionConverter.php',
    'LoggerPatternConverter' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverter.php',
    'LoggerPatternConverterClass' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterClass.php',
    'LoggerPatternConverterCookie' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterCookie.php',
    'LoggerPatternConverterDate' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterDate.php',
    'LoggerPatternConverterEnvironment' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterEnvironment.php',
    'LoggerPatternConverterFile' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterFile.php',
    'LoggerPatternConverterLevel' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterLevel.php',
    'LoggerPatternConverterLine' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterLine.php',
    'LoggerPatternConverterLiteral' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterLiteral.php',
    'LoggerPatternConverterLocation' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterLocation.php',
    'LoggerPatternConverterLogger' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterLogger.php',
    'LoggerPatternConverterMDC' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterMDC.php',
    'LoggerPatternConverterMessage' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterMessage.php',
    'LoggerPatternConverterMethod' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterMethod.php',
    'LoggerPatternConverterNDC' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterNDC.php',
    'LoggerPatternConverterNewLine' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterNewLine.php',
    'LoggerPatternConverterProcess' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterProcess.php',
    'LoggerPatternConverterRelative' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterRelative.php',
    'LoggerPatternConverterRequest' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterRequest.php',
    'LoggerPatternConverterServer' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterServer.php',
    'LoggerPatternConverterSession' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterSession.php',
    'LoggerPatternConverterSessionID' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterSessionID.php',
    'LoggerPatternConverterSuperglobal' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterSuperglobal.php',
    'LoggerPatternConverterThrowable' => $vendorDir . '/apache/log4php/src/main/php/pattern/LoggerPatternConverterThrowable.php',
    'LoggerPatternParser' => $vendorDir . '/apache/log4php/src/main/php/helpers/LoggerPatternParser.php',
    'LoggerReflectionUtils' => $vendorDir . '/apache/log4php/src/main/php/LoggerReflectionUtils.php',
    'LoggerRenderer' => $vendorDir . '/apache/log4php/src/main/php/renderers/LoggerRenderer.php',
    'LoggerRendererDefault' => $vendorDir . '/apache/log4php/src/main/php/renderers/LoggerRendererDefault.php',
    'LoggerRendererException' => $vendorDir . '/apache/log4php/src/main/php/renderers/LoggerRendererException.php',
    'LoggerRendererMap' => $vendorDir . '/apache/log4php/src/main/php/renderers/LoggerRendererMap.php',
    'LoggerRoot' => $vendorDir . '/apache/log4php/src/main/php/LoggerRoot.php',
    'LoggerThrowableInformation' => $vendorDir . '/apache/log4php/src/main/php/LoggerThrowableInformation.php',
    'LoggerUtils' => $vendorDir . '/apache/log4php/src/main/php/helpers/LoggerUtils.php',
    'PDF417' => $vendorDir . '/tecnick.com/tcpdf/include/barcodes/pdf417.php',
    'QRcode' => $vendorDir . '/tecnick.com/tcpdf/include/barcodes/qrcode.php',
    'SessionHandlerInterface' => $vendorDir . '/symfony/http-foundation/Symfony/Component/HttpFoundation/Resources/stubs/SessionHandlerInterface.php',
    'TCPDF' => $vendorDir . '/tecnick.com/tcpdf/tcpdf.php',
    'TCPDF2DBarcode' => $vendorDir . '/tecnick.com/tcpdf/tcpdf_barcodes_2d.php',
    'TCPDFBarcode' => $vendorDir . '/tecnick.com/tcpdf/tcpdf_barcodes_1d.php',
    'TCPDF_COLORS' => $vendorDir . '/tecnick.com/tcpdf/include/tcpdf_colors.php',
    'TCPDF_FILTERS' => $vendorDir . '/tecnick.com/tcpdf/include/tcpdf_filters.php',
    'TCPDF_FONTS' => $vendorDir . '/tecnick.com/tcpdf/include/tcpdf_fonts.php',
    'TCPDF_FONT_DATA' => $vendorDir . '/tecnick.com/tcpdf/include/tcpdf_font_data.php',
    'TCPDF_IMAGES' => $vendorDir . '/tecnick.com/tcpdf/include/tcpdf_images.php',
    'TCPDF_IMPORT' => $vendorDir . '/tecnick.com/tcpdf/tcpdf_import.php',
    'TCPDF_PARSER' => $vendorDir . '/tecnick.com/tcpdf/tcpdf_parser.php',
    'TCPDF_STATIC' => $vendorDir . '/tecnick.com/tcpdf/include/tcpdf_static.php',
    'fpdi_pdf_parser' => $vendorDir . '/setasign/fpdi/fpdi_pdf_parser.php',
    'pdf_context' => $vendorDir . '/setasign/fpdi/pdf_context.php',
    'pdf_parser' => $vendorDir . '/setasign/fpdi/pdf_parser.php',
);
