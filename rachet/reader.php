<?php
  /**
   * Created by PhpStorm.
   * User: redroger
   * Date: 11/1/14
   * Time: 10:02 AM
   */

  require __DIR__ . '/vendor/autoload.php';
  require dirname(__DIR__) . "/configs/easy/rachet/constants.php";

  $shortOpts = "j:f:u::p::";
  $longOpts = array(
    "job:",     // Required: job
    "uid::",     // Options: directory
    "file:",     // Options: file
    "page::",     // Options: page
  );
  $options = getopt($shortOpts, $longOpts);
  //assign
  foreach ($options as $key => $val) $$key = $val;

  $proceed = isset($job) && isset($file) ? true : (isset($j) && isset($f) ? true : false);

  if (!$proceed)
    throw new Exception('missing options --job required, --file required');

  //needed variables
  $job = isset($job) ? $job : $j;
  $uid = isset($uid) ? $uid : (isset($u) ? $u : false);
  $filename = isset($file) ? $file : $f;
  $page = isset($page) ? $page : (isset($p) ? $p : 1);

  if ($uid && empty($uid))
    throw new Exception('--uid is required');


  /***************************************************************************************************
   *  RUNNING THE SCRIPT
   ***************************************************************************************************/
  use Easy\Models\MongoConnect;


  Logger::configure(CONFIGS . '/loggerConfig.xml');
  MongoConnect::configure(CONFIGS . '/environment.php');

  $easy = new \Easy\Controllers\Easy();


  switch (strtolower($job)) {
    case 'count':
      /**
       * Must provide the --file path, it will read the file create a copy file, some pdf files may have bad binary data
       * which can result in bad reads.
       *
       */
      $result = $easy->getFileInfo($filename);
      break;
    case 'explode':
      $result = $easy->explodeFile($filename, $page);
      break;
    case 'read':
      $result = $easy->readFile($filename);
      break;
    case 'extract':
//		$result = $easy->extractFileContents($filename);
      break;
  }

  echo json_encode($result);
