# Easy Controller

Read PDF file and split individual PDF if multiple,
Then create a HIRES TIFF image, using Ghost command
The TIFF image will be then read through tesseract-ocr
to create a text file, of the read PDF's


@package FPDF required http://www.fpdf.org/
@package FPDI required http://www.setasign.de/products/pdf-php-solutions/fpdi/
@package tesseract-ocr required https://code.google.com/p/tesseract-ocr/
@package ghostscript required https://launchpad.net/ubuntu/+source/ghostscript
@package imageMagic required http://www.imagemagick.org/script/convert.php


## Installation

The following dependencies are needed for Easy Software 

```sh
apt-get install tesseract-ocr
apt-get install ghostscript


