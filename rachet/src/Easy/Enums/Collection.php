<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 4/19/14
 * Time: 9:13 AM
 */

namespace Easy\Enums;


class Collection extends \SplEnum {

	const __default = self::USERS;

	const USERS= "users";
	//individual files
	const FILES = "files";
	//files that are being uploaded
	const UPLOADS = "uploads";

	const FOLDERS = "folder";


}
