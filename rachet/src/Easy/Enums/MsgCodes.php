<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 4/18/14
 * Time: 9:01 PM
 */

namespace Easy\Enums;

final class MsgCodes extends \SplEnum
{

	const __default = self::ACCT_NOT_ACTIVATED;

	const EMAIL_SUCCESS = 10;
	const EMAIL_SEND = 20;
	const EMAIL_ADDRESS_EXIST = 30;
	const USERNAME_EXIST = 40;
	const NEW_USER_ADDED = 50;
	const FAILED_ADD_USER = 60;
	const FAILED = 61;
	const ACCT_NOT_ACTIVATED = 70;
	const MISSING_PARAMETERS = 80;
	const MISSING_FILE = 85;
	const INCORRECT_USERNAME_PASSWORD = 90;
	const USER_AUTH_EXPIRED = 100;
	const NOT_ENOUGH_CHARS = 101;
	const SEARCH_EMPTY = 102;
	const DUPLICATE = 103;
	const USER_MISSING = 110;
	const SUCCESS = 200;
	const FOUND = 303;
	const REDIRECT = 303;
	const BAD_REQUEST = 400;
	const FORBIDDEN = 403;
	const NOT_FOUND = 404;
	const SERVER_ERROR = 500;
	const NEW_ENTRY = 600;
	const ENTRY_EXIST = 610;
	const FAILED_COPY = 620;

	public static function enum($value, $options, $default = '')
	{

		if ($value !== null) {
			if (array_key_exists($value, $options)) {
				return $options[$value];
			}
			return $options[$default];
		}
		return "NOT_SET";

	}

	public static function msg_toString($value = null)
	{
		$options = array(
			self::EMAIL_SUCCESS => "EMAIL_SUCCESS",
			self::EMAIL_SEND => "EMAIL_SEND",
			self::EMAIL_ADDRESS_EXIST => "EMAIL_ADDRESS_EXIST",
			self::USERNAME_EXIST => "USERNAME_EXIST",
			self::NEW_USER_ADDED => "NEW_USER_ADDED",
			self::FAILED_ADD_USER => "FAILED_ADD_USER",
			self::FAILED => "FAILED",
			self::ACCT_NOT_ACTIVATED => "ACCT_NOT_ACTIVATED",
			self::MISSING_PARAMETERS => "MISSING_PARAMETERS",
			self::MISSING_FILE => "MISSING_FILE",
			self::INCORRECT_USERNAME_PASSWORD => "INCORRECT_USERNAME_PASSWORD",
			self::USER_AUTH_EXPIRED => "USER_AUTH_EXPIRED",
			self::NOT_ENOUGH_CHARS => "NOT_ENOUGH_CHARS",
			self::DUPLICATE => "DUPLICATE",
			self::USER_MISSING => "USER_MISSING",
			self::SUCCESS => "SUCCESS",
			self::FOUND => "FOUND",
			self::REDIRECT => "REDIRECT",
			self::BAD_REQUEST => "BAD_REQUEST",
			self::FORBIDDEN => "FORBIDDEN",
			self::NOT_FOUND => "NOT_FOUND",
			self::SERVER_ERROR => "SERVER_ERROR",
			self::NEW_ENTRY => "NEW_ENTRY",
			self::ENTRY_EXIST => "ENTRY_EXIST",
			self::FAILED_COPY => "FAILED_COPY",
		);

		return self::enum($value, $options, self::ACCT_NOT_ACTIVATED);
	}

}
