<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 4/19/14
 * Time: 9:40 AM
 */

namespace Easy\Enums;


class LogMsg extends \SplEnum {

	const __default = self::ACCT_NOT_ACTIVATED;

	const MISSING_COLLECTION = ":missing collection:";
	const MONGO_CONN = ":connecting to mongo server:";
	const MISSING_PARAMS = ":missing parameters:";
	const DUPLICATE_KEY = ":duplicate key:";


}