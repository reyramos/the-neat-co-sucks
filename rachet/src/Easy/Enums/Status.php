<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 4/19/14
 * Time: 9:13 AM
 */

namespace Easy\Enums;


class Status extends \SplEnum
{

	const __default = self::PENDING;

	const PENDING = "PENDING";
	const INITIALIZE = "INITIALIZE";
	const READING = "READING";
	const COMPLETED = "COMPLETED";
	const FAILED = "FAILED";
	const PAUSED = "PAUSED";
	const NEW_PAGE = "NEW_PAGE";
	const UPLOADING = "UPLOADING";
	const UPLOAD_COMPLETE = "UPLOAD_COMPLETE";
	const NEW_FILE = "NEW_FILE";
	const READ_FILE = "READ_FILE";
	const COUNT = "COUNT";

}