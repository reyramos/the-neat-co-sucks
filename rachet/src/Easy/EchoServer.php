<?php

namespace Easy;

use Easy\Enums\Status;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use Easy\Controllers\Easy;

class EchoServer implements MessageComponentInterface {
	protected $clients;
	protected $reader;

	public function __construct() {
		$this->clients = array();
		$this->reader = array();


		echo "http://localhost:9999==================\n";
		echo " _       _ _      _                    \n";
		echo "| |_ ___| | |_   | |_ ___    _____ ___ \n";
		echo "|  _| .'| | '_|  |  _| . |  |     | -_|\n";
		echo "|_| |__,|_|_,_|  |_| |___|  |_|_|_|___|\n";
		echo "             the neat co software sucks\n";
		echo "                                       \n";
		echo "=======================================\n";


	}

	public function onOpen(ConnectionInterface $conn) {
		// Store the new connection to send messages to later
		$this->clients[$conn->resourceId] = $conn;
		$this->reader[$conn->resourceId] = new Easy();

		echo "New connection! ({$conn->resourceId})\n";
	}

	public function onMessage(ConnectionInterface $conn, $msg) {
		$data = json_decode($msg);

		switch ($data->request) {
			case Status::COUNT:
				$result = $this->reader[$conn->resourceId]->getFileInfo($data->filename);
				break;
			case Status::READ_FILE:
				$result = $this->reader[$conn->resourceId]->readFile($data->page);
				break;
		}

		$conn->send(json_encode($result));

	}

	public function onClose(ConnectionInterface $conn) {
		// The connection is closed, remove it, as we can no longer send it messages
		unset($this->clients[$conn->resourceId]);
		echo "Connection {$conn->resourceId} has disconnected\n";
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		echo "An error has occurred: {$e->getMessage()}\n";

		$conn->close();
	}
}
