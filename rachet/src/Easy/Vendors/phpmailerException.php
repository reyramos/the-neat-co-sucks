<?php
/**
 * PHPMailer exception handler
 * @package PHPMailer
 */

namespace Easy\Vendors;

class phpmailerException extends \Exception
{

	private $logger;


	public function __construct(){
		$this->logger = \Logger::getLogger(__CLASS__);

	}

	/**
	 * Prettify error message output
	 * @return string
	 */
	public function errorMessage()
	{

		$errorMsg = '<strong>' . $this->getMessage() . "</strong><br />\n";
		$this->logger->error($errorMsg);
		return $errorMsg;
	}
}