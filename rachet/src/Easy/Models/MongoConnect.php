<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 4/11/14
 * Time: 7:54 PM
 */

namespace Easy\Models;

use Easy\Enums\LogMsg;
use Easy\Enums\MsgCodes;

class MongoConnect
{

	protected static $config;
	protected $mongo;
	protected $db;
	//private
	private $MongoExtensions = null;
	private $log;
	private $insertedID;

	public static function configure($configuration)
	{


		if (file_exists($configuration)) {
			self::$config = include_once $configuration;
		} else {
			//some error
		}

	}


	public function __construct()
	{

		$this->log = \Logger::getLogger(__CLASS__);

		$security = self::$config['db']['security'];
		try {

			//TODO:: database authentication
//			$this->mongo = new \MongoClient("mongodb://" . $security['username'] . ":" . $security['password'] . "@" . self::$config['db']['host'] . ":" . self::$config['db']['port']);
			$this->mongo = new \MongoClient("mongodb://" . self::$config['db']['host'] . ":" . self::$config['db']['port']);

		} catch (\MongoConnectionException $e) {

			die($this->log->error(__METHOD__ . LogMsg::MONGO_CONN));

		} catch (\MongoException $e) {

			die($this->log->error(__METHOD__ . $e->getMessage()));

		}

	}

	public function MongoFind($array, $collection)
	{

		//check collection config exist
		if ($this->collectionExist($collection)) {
			//getting database name
			$database = $this->getDatabase($collection);

			//connecting to database
			$this->db = $this->mongo->$database;

			$cursor = $this->db->$collection;
			$result = $cursor->findOne($array);

//			$this->createIndexing($collection);

			return $result;

		}
	}




	/**
	 * @param string $collection
	 * @param array $function |$array, Javascript function to reduce results
	 *  Ex: array('$where',function()
	 *              return this.name == 'Joe' || this.age == 50;
	 * ))
	 * @return \MongoCursor
	 */
	public function MongoFetchAll($collection = "", $function = array())
	{
		//check collection config exist
		if ($this->collectionExist($collection)) {
			//getting database name
			$database = $this->getDatabase($collection);
			//connecting to database
			$this->db = $this->mongo->$database;

			$cursor = $this->db->$collection;
			$result = $cursor->find($function);


			$this->getMongoExtensions($result);

			return $this->cleanResults($result);

		}
	}


	/**
	 * Set Mongo Object extended functionality
	 * @param array $functions
	 */
	public function setMongoCursor($functions = array())
	{
		$this->MongoExtensions = $functions;
	}

	private function getMongoExtensions($obj)
	{

		//if this does not exit
		if (empty($this->MongoExtensions))
			return;

		foreach ($this->MongoExtensions as $mongo => $arr) {
			//MongoCursor
			if (is_object($obj) && $obj instanceof \MongoCursor && $mongo == "MongoCursor") {
				if (is_array($arr)) {
					foreach ($arr as $func => $v) {
						$obj->$func($v);
					}
				}
			}

			if (is_object($obj) && $obj instanceof \MongoCollection && $mongo == "MongoCollection") {
				//todo:something for MongoCollection
			}

		}
	}


	/**
	 * @param $collection
	 * @return \MongoCollection
	 */
	public function MongoCollection($collection)
	{
		if ($this->collectionExist($collection)) {
			//getting database name
			$database = $this->getDatabase($collection);
			//connecting to database
			$this->db = $this->mongo->$database;

			return $this->db->$collection;
		}
	}

	/**
	 * Insert Data into the database
	 * @param $array
	 * @param $collection
	 * @return bool
	 */
	public function MongoInsert($array, $collection)
	{

		try {
			$array['_id'] = new \MongoId();
		} catch (\MongoException $e) {
			$this->log->error($e->getMessage());
		}

		//check collection config exist
		if ($this->collectionExist($collection)) {

			//getting database name
			$database = $this->getDatabase($collection);

			//connecting to database
			$this->db = $this->mongo->$database;
			$table = $collection;
			$this->createIndexing($collection);

			$collection = $this->db->$table;

			try {
				$collection->insert($array);
				$this->insertedID = (string)$array['_id'];
			} catch (\MongoCursorException $e) {
				$this->log->error('MongoException::' . $e->getMessage());
				return false;
			} catch (\MongoException $e) {
				$this->log->error('MongoCursorException::' . $e->getMessage());
				return false;
			}
			return true;
		}
	}

	public function getInsertedId()
	{
		return $this->insertedID;
	}

	private function collectionExist($collection)
	{

		if (array_key_exists($collection, self::$config['collections'])) {
			return true;
		} else {
			$this->log->error(__METHOD__ . LogMsg::MISSING_COLLECTION . $collection);
			return false;
		}
	}


	public function MongoUpdate(\MongoId $find, $update, $collection)
	{

		$find = array('_id' => $find);

		if ($this->collectionExist($collection)) {

			//getting database name
			$database = $this->getDatabase($collection);

			//connecting to database
			$this->db = $this->mongo->$database;

			$this->createIndexing($collection);

			$table = $collection;
			$collection = $this->db->$table;
			$result = $collection->update($find, array('$set' => $update));

			return $result;
		}
	}


	/**
	 * Return a clean result of Mongo Results
	 * @param \MongoCursor $cursor
	 * @return array
	 */
	protected function cleanResults(\MongoCursor $cursor)
	{
		$handler = array();

		if ($cursor->count()):
			foreach ($cursor as $object) {
				$temp = array();

				foreach ($object as $k => $v) {
					$array = array($k => is_string($v) ? preg_replace('/[^(\x20-\x7f)]*/s', '', htmlspecialchars_decode($v)) : $v);
					//if this is a mongo id object
					if (is_object($v) && $v instanceof \MongoId) {
						$array = array($k => $v->{'$id'});
					}

					$temp = array_merge($temp, $array);
				}
				array_push($handler, $temp);
			}
		endif;

		return $handler;
	}

	/**
	 * Return the default database collection or define by config
	 * @param string $collection
	 * @return mixed
	 */
	private function getDatabase($collection = "")
	{
		return array_key_exists('db', self::$config['collections'][$collection]) ?
			array_key_exists('name', self::$config['collections'][$collection]['db']) ? self::$config['collections'][$collection]['db']['name'] : self::$config['db']['name'] : self::$config['db']['name'];
	}

	/**
	 * @param string $collection
	 */
	private function createIndexing($collection = "")
	{
		if (array_key_exists('ensureIndex', self::$config['collections'][$collection])) {
			$index = new \MongoCollection($this->db, $collection);
			foreach (self::$config['collections'][$collection]['ensureIndex'] as $key => $val) {
				$index->createIndex(self::$config['collections'][$collection]['ensureIndex'][$key]["key"], self::$config['collections'][$collection]['ensureIndex'][$key]["type"]);
			}
		}
	}


	/**
	 * automagick function,
	 * called on destroy of the instance
	 * reset the db handle for garbage collection
	 */
	public function __destruct()
	{
		$this->mongo = null;
	}
}
