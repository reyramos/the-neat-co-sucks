<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 2/16/14
 * Time: 1:34 PM
 */

namespace Easy\Models;


use Easy\Enums\MsgCodes;

final class  Utilities
{
	private $key = 'qJB0rGtIn5UB1xG03efyCp';
	protected $response = array();
	static $inst = null;
	public static function Instance()

	{

		if (self::$inst == null) {

			self::$inst = new Utilities();
		}

		return self::$inst;
	}

	/* NOT TO BE USED */
	function __construct(){}

	/**
	 * Pass string, integer value to determine boolean
	 * @param $resolve
	 * @return bool
	 */
	public function getBoolean($resolve)
	{
		if (is_string($resolve)) {
			return preg_match("/false|no|off/i", $resolve) ? false : true;
		}

		if (is_integer($resolve)) {
			return $resolve > 0 ? true : false;
		}

		if (is_bool($resolve)) {
			return $resolve;
		}

		return is_null($resolve) ? false : true;

	}

	public function encryptIt($q)
	{
		$cryptKey = $this->key;
		$qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
		return ($qEncoded);
	}

	public function decryptIt($q)
	{
		$cryptKey = $this->key;
		$qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
		return ($qDecoded);
	}

	public function setResponse($msg){

		if(!array_key_exists('code',$this->response))
		$this->response['code'] = is_numeric($msg)?array("id"=>$msg,"message"=>MsgCodes::msg_toString($msg)):array("id"=>MsgCodes::SUCCESS,"message"=>MsgCodes::msg_toString(MsgCodes::SUCCESS));

		$this->response['data'] = $msg;

	}

	public function setResponseCode($msg){
		$this->response['code'] = array("id"=>$msg,"message"=>MsgCodes::msg_toString($msg));
	}

	public function success($data = null)
	{

		// use the global response object
		// this way, other methods can add to it if needed
		$this->response['STATUS'] = 'OK';

		if($data != null)
		$this->response['data'] = $data;

		die(json_encode($this->response));
	}

	public function failure($data = null)
	{
		// use the global response object
		// this way, other methods can add to it if needed
		$this->response['STATUS'] = 'FAILED';
		if ($data != null)
			$this->response['data'] = $data;

		die(json_encode($this->response));
	}

}


