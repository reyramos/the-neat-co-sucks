<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 4/11/14
 * Time: 7:51 PM
 */

namespace Easy\Models;

//mongo connection
//use Easy\Models\MongoConnect;


use Easy\Enums\Collection;
use Easy\Enums\MsgCodes;

class User extends MongoConnect
{

	//hash encryption for token
	private $algo = "ripemd256";
	//login timeStamp
	private $dt = "Y-m-d G:i";
	//set #hrs for token expiration
	private $expire = array('Hours' => 6);
	private $dateTime_array = array();

	private $log;
	private $util;

	public function __construct()
	{

		parent::__construct();
		$this->log = \Logger::getLogger(__CLASS__);
		$this->util = Utilities::Instance();
	}

	/**
	 * Finds the username and compares password with MongoDB,
	 * Sets new Token, return token and authentication information
	 *
	 * @param $array
	 * @param $password
	 * @return array|bool|null
	 */
	protected function getUserAuth($array, $password)
	{

		unset($array['password']);

		$cursor = $this->MongoFind($array, Collection::USERS);
		if ($cursor == null) {
			$this->util->setResponse(MsgCodes::USER_MISSING);
			return false;
		}

		$obj = $cursor;
		$obj['_id'] = $cursor['_id']->{'$id'};

		unset($obj['password']);

		$obj['token'] = $this->createTokenHash($obj['_id']);

		if (password_verify($password, $cursor['password'])) {

			//for setting false date
			$debugDate = false ? strtotime("2014-04-12 08:14:24") : strtotime(date($this->dt));

			$update = array('token' => $obj['token'], "started" => new \MongoDate($debugDate));

			$result = parent::MongoUpdate(array('_id' => new \MongoId($obj['_id'])), $update, Collection::USERS);

			if (!$result['err'] && $result['updatedExisting']) {
				return $obj;
			} else {

				$this->util->setResponse(MsgCodes::INCORRECT_USERNAME_PASSWORD);
				return false;
			}

		} else {

			$this->util->setResponse(MsgCodes::INCORRECT_USERNAME_PASSWORD);
			return false;
		}

	}

	protected function getUser($array)
	{

		$cursor = $this->MongoFind($array, Collection::USERS);
		$obj = $cursor;

		if ($cursor) {
			if ($this->is_expired(date($this->dt, $obj['started']->sec))) {
				$this->util->setResponse(MsgCodes::USER_AUTH_EXPIRED);
				return false;
			} else {
				$obj['_id'] = $cursor['_id']->{'$id'};

				//remove unnecessary information
				unset($obj['password']);
				unset($obj['started']);
				unset($obj['token']);

				return $cursor ? $obj : false;
			}
		}

		return $cursor;


	}


	/**
	 * Check if date time has expired
	 * @param $dt [string]
	 * @return bool
	 */
	function is_expired($dt)
	{

		$dateDiff = $this->dateDiff($dt, true);
		foreach ($this->expire as $key => $val) {
			if (array_key_exists($key, $dateDiff)) {
				if ($dateDiff[$key] > $this->expire[$key]) return true;
			}
		}

		return false;
	}


	/**
	 *
	 * Create array of date Difference
	 *
	 * @param string $end
	 * @param bool $out_in_array
	 * @return array|string
	 */

	function dateDiff($end = '2020-06-09 10:30:00', $out_in_array = false)
	{
		$intervalo = date_diff(date_create(), date_create($end));
		$out = $intervalo->format("Years:%Y,Months:%M,Days:%d,Hours:%H,Minutes:%i,Seconds:%s");

		$o_array = explode(',', $out);
		if (!$out_in_array)
			return $out;

		$new_out = array();
		$callback = function ($val, $key) use ($new_out) {

			$v = explode(':', $val);
			$this->dateTime_array = array_merge($this->dateTime_array, array($v[0] => $v[1]));

		};

		array_walk($o_array, $callback);
		return $this->dateTime_array;

	}


	/**
	 * Create new OAuthRequest based on Mongo(_id) and dateTime
	 * @param $_id
	 * @return string
	 */
	private function createTokenHash($_id)
	{
		$token = utf8_encode(hash($this->algo, $_id . date('c')));
		return $token;
	}


}