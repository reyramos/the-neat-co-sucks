<?php


namespace Easy;

use Easy\Controllers\Mediator;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use Easy\Controllers\UploadServer;
use Easy\Enum;


class Chat implements MessageComponentInterface
{
	protected $clients;
	protected $uploader;
	protected $easy;


	public function __construct()
	{
		$this->clients = array();
		$this->easy = array();
		$this->uploader = new UploadServer();

		echo "http://localhost:" . PORT . "\n";
		echo "    ███████╗ █████╗ ███████╗██╗   ██╗\n";
		echo "    ██╔════╝██╔══██╗██╔════╝╚██╗ ██╔╝\n";
		echo "    █████╗  ███████║███████╗ ╚████╔╝ \n";
		echo "    ██╔══╝  ██╔══██║╚════██║  ╚██╔╝  \n";
		echo "    ███████╗██║  ██║███████║   ██║   \n";
		echo "    ╚══════╝╚═╝  ╚═╝╚══════╝   ╚═╝   \n";
		echo "                               Done!!\n";
	}

	public function onOpen(ConnectionInterface $conn)
	{
		$this->clients[$conn->resourceId] = $conn;
		echo "New connection! ({$conn->resourceId})\n";
	}

	public function onMessage(ConnectionInterface $conn, $msg)
	{

		if (preg_match('#^wsFileTransfer:(.*?)$#', $msg, $matches) > 0) {
			$data = json_decode($matches[1]);

			switch (strtolower($data->requestType)) {
				case "init":
					$this->uploader->initializeUpload($conn, $data);
					break;
				case "data":
					// First decode the data received as a base64 string.
					$data = base64_decode($data->data);
					$this->uploader->addData($conn, $data);
					break;
				case "complete":
					$this->uploader->finishUpload($conn);
					$this->easy[$conn->resourceId] = new Mediator();

					// Sends a response to the client
					$response = array(
						'type' => 'STAT',
						'code' => 200,
						'request' => 'COUNT',
						'filename' => $data->filename
					);

					$msg = json_encode($response);
					$talkBack = $this->easy[$conn->resourceId]->request($msg);

					$conn->send($msg);
					$conn->send($talkBack);


					break;
			}
		} else {
			echo "$msg\n";

			//TODO: different project to remove
		}
	}

	public function onClose(ConnectionInterface $conn)
	{
		// The connection is closed, remove it, as we can no longer send it messages
		unset($this->clients[$conn->resourceId]);


		echo "Connection {$conn->resourceId} has disconnected\n";
	}

	public function onError(ConnectionInterface $conn, \Exception $e)
	{
		echo "An error has occurred: {$e->getMessage()}\n";

		$conn->close();
	}
}