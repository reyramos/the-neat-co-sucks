<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 10/28/14
 * Time: 9:28 PM
 */

namespace Easy\Controllers;


class Mediator {

	private $host = "127.0.0.1";
	private $port = 8999;
	private $socket_handler;

	function __construct(){
		$this->socket_handler = socket_create(AF_INET, SOCK_STREAM, 0);
		$result = socket_connect($this->socket_handler, $this->host, $this->port);

	}

	/**
	 * Send a command to Talk To Me Server
	 * @param $cmd
	 * @return bool|string
	 */
	function request($cmd){
		$int = socket_write($this->socket_handler, $cmd, strlen($cmd));
		if($int){
			$socket_reply = socket_read($this->socket_handler, 1000000000);
			return $socket_reply;
		}
		return false;
	}

	function done(){
		socket_close($this->socket_handler);
	}

} 