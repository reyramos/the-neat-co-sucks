<?php
  /**
   * Read PDF file and split individual PDF if multiple,
   * Then create a HIRES TIFF image, using Ghost command
   * The TIFF image will be then read through tesseract-ocr
   * to create a text file, of the read PDF's
   *
   *
   * @package FPDF required http://www.fpdf.org/
   * @package FPDI required http://www.setasign.de/products/pdf-php-solutions/fpdi/
   * @package tesseract-ocr required https://code.google.com/p/tesseract-ocr/
   * @package ghostscript required https://launchpad.net/ubuntu/+source/ghostscript
   * @package imageMagic required http://www.imagemagick.org/script/convert.php
   *
   */

  namespace Easy\Controllers;

  use Easy\Enums\MsgCodes;
  use Ratchet\ConnectionInterface;

  use Easy\Models\MongoConnect;
  use Easy\Models\Utilities;
  use Easy\Enums\Collection;
  use Easy\Enums\Status;


  class Easy extends MongoConnect {

    private $log;
    private $util;
    private $filename;
    private $uid;

    public function __construct()
    {

      parent::__construct();

      $this->log = \Logger::getLogger(__CLASS__);
      $this->util = Utilities::Instance();

    }

    /**
     * Get the Number pages from the PDF file
     *
     * @param $filename , file path and file name
     *
     * @return array
     */
    function getFileInfo($filename)
    {

      $path_parts = pathinfo($filename);

      if (!file_exists($path_parts['dirname'] . "/.tmp"))
        mkdir($path_parts['dirname'] . "/.tmp");


      $this->filename = $filename;

      //get the filepath info
      $path_parts = pathinfo($filename);

      //the file_copy path
      $file_copy = $path_parts['dirname'] . "/" . $path_parts['filename'] . "_copy." . $path_parts['extension'];

      //linux command to first fix the file in case it corrupted.
      exec('cp "' . $filename . '" "' . $file_copy . '"', $output, $return);

      /**
       * Copy the PDF on temp folder to be overridden with GS,
       * Some PDF files have bad compressions on device
       * On copy file success, run GhostScript on same file
       * Then execute FPDI
       */

      if (!$return) {
        shell_exec('gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile="' . $filename . '" "' . $file_copy . '"');
        unlink($file_copy); //remove the file
        // initiate FPDI
        $FPDI = new \FPDI();
        // get the page count
        $pageCount = $FPDI->setSourceFile($filename);


        // import a page
        $templateId = $FPDI->importPage(1);

        // get the size of the imported page
        $size = $FPDI->getTemplateSize($templateId);


      } else {
        $pageCount = "EASY.GET_FILE_INFO:" . MsgCodes::msg_toString(620);
      }

      // Sends a response to the client
      $result = array(
        'broadcast' => 'wsFileTransfer',
        'code'      => 200,
        'pages'     => $pageCount,
        'filename'  => $filename,
        'request'   => Status::COUNT,
        'size'      => $size,
      );

      return $result;
    }


    /**
     * Extracts the files from the file. If the PDF has multiple pages it will extract all the individual pdf's files
     *
     * @param $filename , filename path
     * @param $page     , create the new page from page number
     *
     * @return array|void
     * @throws Exception
     */
    function explodeFile($filename, $page)
    {

      $path_parts = pathinfo($filename);

      $this->filename = $filename;

      // initiate FPDI
      $FPDI = new \FPDI();

      $FPDI->setSourceFile($filename);

      // import a page
      $templateId = $FPDI->importPage($page);

      // get the size of the imported page
      $size = $FPDI->getTemplateSize($templateId);

      //create a new pdf file for every page
      $new_pdf = new \FPDI();
      // create a page (landscape or portrait depending on the imported page size)
      if ($size['w'] > $size['h']) {
        $new_pdf->AddPage('L', array($size['w'], $size['h']));
      } else {
        $new_pdf->AddPage('P', array($size['w'], $size['h']));
      }

      $new_pdf->setSourceFile($filename);
      $new_pdf->useTemplate($new_pdf->importPage($page));


      $insert = array(
        "parent_file" => basename($filename, ".pdf"),
        "page"        => $page,
        "size"        => 0
      );

      $new_filename = $this->insert($insert);

      //if there is no filename return false
      if (!$new_filename) return;


      try {
        $new_pdf->Output($path_parts['dirname'] . "/" . $new_filename . ".pdf", "F");

        $this->update(new \MongoId($new_filename),
          array(
            "size"     => filesize($path_parts['dirname'] . "/" . $new_filename . ".pdf"),
            "filename" => $new_filename . ".pdf"
          )
        );

        $outputTempFileName = $path_parts['dirname'] . "/.tmp/" . $new_filename . "_temp.jpg";
        $outputTiffFileName = $path_parts['dirname'] . "/.tmp/" . $new_filename . ".jpg";
        $inputPDFFileName = $path_parts['dirname'] . "/" . $new_filename . ".pdf";

        //ghost script command to run
//      $cmd = "gs -SDEVICE=tiffg4 -r600x600 -sPAPERSIZE=letter -sOutputFile=$outputTiffFileName -dNOPAUSE -dBATCH  $inputPDFFileName 2> /dev/null";
        shell_exec("convert -density 300 -trim $inputPDFFileName -quality 100 $outputTempFileName 2> /dev/null");

        $cleaner = APP_PATH . "/scripts/textcleaner -g -e normalize -f 10 -o 5 -t 50 -s 1 $outputTempFileName $outputTiffFileName";
        shell_exec($cleaner);


        unlink($outputTempFileName); //remove the temp file


        //change the permission to all
        chmod($outputTiffFileName, 0777);
        chmod($inputPDFFileName, 0777);

        // Sends a response to the client
        $result = array(
          'broadcast'     => 'wsFileTransfer',
          'code'          => 200,
          'page'          => $page,
          'filename'      => $filename,
          'new_file_name' => $new_filename . ".pdf",
          'status'        => Status::READING
        );

        return $result;

      } catch (\Exception $e) {
        $this->log->error($e->getMessage());
      }

    }

    function readFile($filename)
    {
      $path_parts = pathinfo($filename);

      if (!file_exists($path_parts['dirname'] . "/.txt"))
        mkdir($path_parts['dirname'] . "/.txt");


      $basename = basename($filename, ".pdf");
      $pdfDirectory = $path_parts['dirname'];

      $textFileName = $pdfDirectory . "/.txt/" . $basename . ".txt";

      try {
        $outputTiffFileName = $pdfDirectory . "/.tmp/" . $basename . ".jpg";

        if (!file_exists($outputTiffFileName)) {
          return array(
            'broadcast' => 'wsFileTransfer',
            'code'      => 500,
            'status'    => Status::FAILED . ":" . MsgCodes::msg_toString(MsgCodes::MISSING_FILE)
          );
        }

        //tesseract-ocr command and keep the output quiet
        $tesseract = "tesseract $outputTiffFileName " . $pdfDirectory . "/.txt/" . $basename . " 2> /dev/null";
        exec($tesseract, $output, $return);

        if (!$return) {

          $this->extractFileContents($textFileName);

          $result = array(
            'broadcast' => 'wsFileTransfer',
            'code'      => 200,
            'filename'  => $basename . ".pdf",
            'status'    => Status::COMPLETED
          );

          $content = file_get_contents($textFileName);
          $this->update(new \MongoId($basename),
            array(
              "content"       => $content,
              "text_filename" => $basename . ".txt"
            )
          );

        } else {
          $result = array(
            'broadcast' => 'wsFileTransfer',
            'code'      => 500,
            'filename'  => $basename . ".pdf",
            'status'    => "EASY.READ_FILE:" . MsgCodes::msg_toString(MsgCodes::FAILED_COPY)
          );
        }

        return $result;

      } catch (\Exception $e) {
        $this->log->error($e->getMessage());
      }

    }

    function extractFileContents($filename)
    {

      $basename = basename($filename, ".txt");

      $str = strtolower(file_get_contents($filename));
      $lines = explode("\n", $str);

      $dir = APP_PATH . "/scripts/REGEXs";
      $files = scandir($dir);
      $params = array();

      //get the necessary keys for the regex
      foreach ($this->getParams() as $key => $value) $$key = $value;

      for ($lineNumber = 0; $lineNumber < count($lines); $lineNumber++) {
        $line = strtolower($lines[ $lineNumber ]); //passing data

        //load all the REGEXs files within the directory to run scripts
        foreach ($files as $file) {
          if ($file !== ".." && $file !== "." && !is_dir($dir . "/" . $file)) {

            if (file_exists($dir . "/" . $file))//bring in the REGEX scripts

              include $dir . "/" . $file;

          }
        }

        //expected results from REGEXs files
        $receipts = array(
          "date"   => isset($receipt_date) ? $receipt_date : "",
          "params" => $params
        );
      }

      $this->update(new \MongoId($basename),
        array(
          "regex_data" => $receipts
        )
      );
    }

    /**
     *
     * Creates the necessary data for the regex/search parameters
     *
     * Return the following min, max, keywords
     * @return array
     */

    private function getParams()
    {
      $keywords = include_once(dirname(CONFIGS) . "/regex/search.php");


      uksort($keywords, function ($a, $b) {
        return strlen($b) - strlen($a);
      });

      $min = 0;
      $count = 0;
      foreach ($keywords as $key => $keyword) {
        $handler = $keyword;
        usort($handler, function ($a, $b) {
          return strlen($b) - strlen($a);
        });
        $keywords[ $key ] = $handler;


        if ($count === 0) {
          $max = strlen($key);
        } elseif ($count === (count($keywords) - 1)) {
          $min = strlen($key);
        }


        if (count($keywords[ $key ])) {
          if (strlen($keywords[ $key ][0]) > $max) {
            $max = strlen($keywords[ $key ][0]);
          } elseif (strlen($keywords[ $key ][ count($keywords[ $key ]) - 1 ]) < $min) {
            $min = strlen($keywords[ $key ][ count($keywords[ $key ]) - 1 ]);
          }
        }

        $count++;
      }

      return array("min" => $min, "max" => $max, "keywords" => $keywords);

    }

    private function update(\MongoId $id, $array)
    {
      parent::MongoUpdate($id, $array, Collection::FILES);
    }

    private function insert($array)
    {
      parent::MongoInsert($array, Collection::FILES);

      return parent::getInsertedId();
    }
  }




