<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 10/11/14
 * Time: 4:22 PM
 */

namespace Easy\Controllers;


use Easy\Enums\Status;
use Easy\Models\Utilities;
use Ratchet\ConnectionInterface;

class UploadServer
{

	// Show Debug
	protected $debug = true;

	// All the uploads
	protected $uploads = array();
	protected $ocr = array();

	private $user;
	private $data;
	private $log;
	private $util;


	function __construct()
	{
		$this->log = \Logger::getLogger(__CLASS__);
		$this->util = Utilities::Instance();
	}

	/**
	 * Initializes the upload for a given client
	 * @param $user : the client that is uploading a file
	 * @param $data : the information of the file
	 * @return array
	 */
	function initializeUpload(ConnectionInterface $user, $data)
	{
		$this->data = $data;
		$this->user = $user;
		$this->uploads[$user->resourceId] = new FileUpload($data->filename, $data->size);

		// Sends a response to the client
		$response = array(
			'type' => 'STOR',
			'message' => 'Upload initialized. Wait for data',
			'code' => $this->uploads[$user->resourceId]->getStatus(),
			'filename' => $this->uploads[$user->resourceId]->getFilename()
		);


		$msg = json_encode($response);
		$user->send($msg);
	}

	/**
	 * Adds data to the file for a given client
	 * @param $user : the client
	 * @param $data : the data to add to the file
	 */
	public function addData(ConnectionInterface $user, $data)
	{

		if (isset($this->uploads[$user->resourceId])) {

			$this->uploads[$user->resourceId]->addData($data);

			// Sends a response to the client
			$response = array(
				'type' => 'DATA',
				'code' => 200,
				'bytesRead' => mb_strlen($data),
				'filename' => $this->uploads[$user->resourceId]->getFilename()
			);

			// Sends a response to the client
			$user->send(json_encode($response));
		}
	}


	/**
	 * Finishes the upload of a given client
	 * @param $user : the client
	 */
	public function finishUpload(ConnectionInterface $user)
	{
		if (isset($this->uploads[$user->resourceId])) {
			$this->uploads[$user->resourceId]->close();

			//remove the upload reference
			unset($this->uploads[$user->resourceId]);

			// Sends a response to the client
			$response = array(
				'type' => 'STAT',
				'code' => 200,
				'status' => Status::UPLOAD_COMPLETE
			);

			// Sends a response to the client
			$user->send(json_encode($response));

		}
	}
}