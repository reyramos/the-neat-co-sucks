<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 10/11/14
 * Time: 7:11 PM
 */

namespace Easy\Controllers;

use Easy\Enums\Collection;
use Easy\Enums\MsgCodes;
use Easy\Enums\Status;
use Easy\Models\MongoConnect;

/**
 * Class that represents the upload of a file
 */
class FileUpload extends MongoConnect
{

	//save reference MongoId created
	private $mongoId;

	// The filename of the uploaded file
	private $filename;

	// The size of the uploaded file
	private $size;

	// The ressource to the file
	private $fp;


	private $status;

	/**
	 * Constructs a new File Upload
	 * @param $filename : the name of the file
	 * @param $size : the size of the file
	 */
	public function __construct($filename, $size)
	{

		parent::__construct();//init the database connection

		$date = strtotime(date(DATE_FORMAT));
		$type = substr($filename, stripos($filename, ".") + 1);

		//TODO: update the uid to be a valide user id on site login
		$insert = array(
			Collection::USERS."_id" => "123456",
			"status" => Status::INITIALIZE,
			"path" => UPLOAD_DIR,
			"date_time" => new \MongoDate($date),
			"filename" => $filename,
			"size" => $size,
			"type" => $type
		);

		$result = parent::MongoInsert($insert, Collection::UPLOADS);
		if ($result) {
			$this->mongoId = parent::getInsertedId();
			$filename = $this->mongoId . "." . $type;
			$this->setFilename($filename);
			$this->setSize($size);
			$this->fp = fopen(UPLOAD_DIR . $filename, 'w+');
			$this->status = 200;
		} else {
			$this->status = MsgCodes::DUPLICATE;
		}
	}

	public function getStatus(){
		return $this->status;
	}

	public function getMongoId()
	{
		return $this->mongoId;
	}

	/**
	 * Updates the name of the file
	 * @param $filename : the new name
	 */
	public function setFilename($filename)
	{
		$this->filename = $filename;
	}

	public function getFilename()
	{
		return $this->filename;
	}

	/**
	 * Updates the size of the file
	 * @param $size : the new size of the file
	 */
	public function setSize($size)
	{
		$this->size = $size;
	}

	/**
	 * Adds data to the file
	 * @param $data : the data to add to the file
	 */
	public function addData($data)
	{
		parent::MongoUpdate( new \MongoId($this->mongoId), array("status" => Status::UPLOADING), Collection::UPLOADS);
		fwrite($this->fp, $data);
	}

	/**
	 * Close the file
	 */
	public function close()
	{
		parent::MongoUpdate(new \MongoId($this->mongoId), array("status" => Status::COMPLETED), Collection::UPLOADS);
		fclose($this->fp);
		//close database connection
		parent::__destruct();
	}
}