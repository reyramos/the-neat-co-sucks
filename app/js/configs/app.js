/**
 * Created by redroger on 11/24/14.
 */

(function($) {

    'use strict';

    var appConfig = {
        menu_speed: 200,
        sound_path: "sound/",
        sound_on: true
    };


    $.extend(appConfig);

})(jQuery)
