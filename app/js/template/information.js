/**
 * Created by redroger on 1/25/2015.
 */
angular.module('app')
  .run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('template/tabs/tab.html',
        '<li ng-class="{active: active, disabled: disabled}">' +
        '<a href ng-click="select()" tab-heading-transclude>{{heading}}</a>' +
        '</li>'
      )
    }
  ])
  .run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('template/tabs/tabset.html',
        '<div>' +
        '<ul class="nav nav-{{type || \'tabs\'}} info-tabs" ng-class="{\'nav-stacked\': vertical,\'nav-justified\': justified}" ng-transclude></ul>' +
        '<div class="tab-content">' +
        '<div class="tab-pane" ng-repeat="tab in tabs" ng-class="{active: tab.active}" tab-content-transclude="tab"></div>' +
        '</div>' +
        '</div>'
      )
    }
  ])

