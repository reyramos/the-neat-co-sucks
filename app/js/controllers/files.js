'use strict';

/**
 * # Application
 *
 * Core Application controller that includes functions used before we kickStart the Application
 * The functions store within this files live outside of the ngView and are used as global function
 */
angular.module('app').controller(
  'filesController', [
    '$window'
    , '$scope'
    , '$log'
    , '$timeout'
    , '$location'
    , 'filesFactory'
    , 'utilities'
    , function ($window
      , $scope
      , $log
      , $timeout
      , $location
      , filesFactory
      , utilities) {

      var logger = $log.getInstance('filesController');


      var iframe = $('#frameBox').find("[name='links']")

      logger.info('iframe',iframe)




    }]
)
