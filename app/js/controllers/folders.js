'use strict';
/**
 * # Application
 *
 * Core Application controller that includes functions used before we kickStart the Application
 * The functions store within this files live outside of the ngView and are used as global function
 */
angular.module('app').run(
  [
    '$templateCache',
    function ($templateCache) {
      $templateCache.put(
        '/sidebar/nameFolder.html', '<div class="modal-box aside-modal">' + ' <div class="panel panel-primary">' + '    <div class="panel-heading">' + '      <h3 class="panel-title">Name Folder</h3>' + '   </div>' + ' <div class="panel-body">' + '   <input type="text" class="form-control" id="nameFolder" placeholder="New Folder" ng-model="newFolder.name" required>' + ' </div>' + '   <div class="panel-footer">' + '   <button type="button" class="btn btn-primary" data-ng-click="modal.create()">Create</button>' + '   <button type="button" class="btn btn-default" data-ng-click="modal.cancel()">Cancel</button>' + '   </div>' + ' </div>' + '</div>'
      );
    }
  ]
).controller(
  'foldersController', [
    '$window',
    '$scope',
    '$log',
    '$timeout',
    'folderFactory',
    'modalFactory',
    'appData',
    '$rootScope',
    'filesFactory',
    'loaderService',
    function ($window, $scope, $log, $timeout, folderFactory, modalFactory, appData, $rootScope, filesFactory, loaderService) {
      var logger = $log.getInstance('foldersController'),
          active_cItemMenu = null,
          enums = appData.enums();

      $scope.folderList = folderFactory.getFolders() || [];
      $scope.newFolder = {};

      $scope.easyDrive = function (event) {
        filesFactory.setFiles(appData[enums.GET_CONTENT].data.files)

        var _this = $(event.currentTarget);
        _this.parents().find("li").removeClass('active')

      }

      /**
       * Get the clicked folder id
       * @param _id
       */
      $scope.folderSelector = function (_id) {

        var files = $rootScope['DynamicJarvisObjectParse'][_id].obj

        if (files.length) {
          loaderService.start({
            includeSpinner: false,
            includeBar:false,
            parentSelector: $('#mainPage .c-Header tbody > tr > td'),
            message: '<div class="list-loader"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>'
          })
        }

        $timeout(function () {
          logger.info('folderSelector', _id)
          filesFactory.setFiles($rootScope['DynamicJarvisObjectParse'][_id].obj)
        }, 100)

      }


      function nameFolder(call, name) {
        modalFactory.show({
          template: '/sidebar/nameFolder.html',
          speed: 650,
          modal: {
            create: function () {
              var newFolder = "undefined";
              if (typeof($rootScope.newFolder) === "object") {
                newFolder = $rootScope.newFolder.name;
                $rootScope.newFolder.name = ""; //reset the name
              }
              var data = {
                name: newFolder,
                _id: name || 0
              }

              folderFactory.call(call, data).then(
                function (data) {
                  logger.info(call + '0100', data)
                  //update the instance
                  appData.update(enums.GET_FOLDER).then(
                    function (folders) {
                      $scope.safeApply(
                        function () {
                          //update the folderList array
                          $scope.folderList = folderFactory.getFolders();
                          //rebuild the menu
                          $scope.$broadcast('directive:dynamicJarvis')
                        }
                      )
                    }
                  );
                },
                function (response) {
                  logger.error(response)
                  appData.getError(response.message.name, response.message.code).then(
                    function (msg) {
                      $.smallBox({
                        title: response.message.name + "::" + response.message.code,
                        content: "<i class='fa fa-clock-o'></i> <i>" + msg + "</i>",
                        color: "#C46A69",
                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                        timeout: 4000
                      });
                    }
                  )
                }
              );
              modalFactory.hide()
            },
            cancel: function () {
              modalFactory.hide()
            }
          }
        })
      }


      /**
       * Set the control for sidebar jarvisMenu
       * @type {{closedSign: string, openedSign: string, collapse: boolean}}
       */
      $scope.jarvisMenu = {
        closedSign: '<i class="fa fa-plus-square"></i>',
        openedSign: '<i class="fa fa-minus-square"></i>',
        accordion: false
      }
      /**
       * Set the option of the side bar for contextMenu
       * @type {{ul: *[], onContext: Function, callback: Function}}
       */
      $scope.contextMenu = {
        ul: [
          [{
            name: 'Add new folder',
            icon: 'fa fa-plus-circle',
            call: "addFolder"
          }],
          [{
            name: 'Upload new file',
            icon: "fa fa-upload",
            call: "uploadFile"
          }]
        ],
        onContext: function (element, event) {
          if (active_cItemMenu) {
            active_cItemMenu.removeClass('hover')
          }
        },
        callback: function (call, name) {
          if (call === "uploadFile") {
            modalFactory.show({
              template: '/modal/dropZone.html'
            })
          } else {
            nameFolder(call, name);
          }
        }
      }
      /**
       * Set the contextMeu options for the ul.list element
       * @type {{ul: *[], callback: Function, onContext: Function, offContext: Function}}
       */
      $scope.contextItem = {
        ul: [
          [{
            name: 'Add new folder',
            icon: 'fa fa-plus-circle',
            call: "addFolder"
          }, {
            name: 'Delete folder',
            icon: 'fa fa-minus-circle',
            call: "deleteFolder"
          },],
          [{
            name: 'Upload new file',
            icon: "fa fa-upload",
            call: "uploadFile"
          }, {
            name: 'Rename folder',
            icon: "fa fa-pencil-square-o",
            call: "renameFolder"
          }]
        ],
        callback: function (call, name) {
          if (call === "uploadFile") {
            modalFactory.show({
              template: '/modal/dropZone.html'
            })
          } else {
            switch (call) {
              case 'addFolder':
                nameFolder(call, name);
                break;
              case 'deleteFolder':

                break;
              case 'renameFolder':

                break;
            }
          }
        },
        onContext: function (element, event) {
          //save this active element, to remove later
          active_cItemMenu = $(event.target.parentNode);
          var parent = $(event.target.parentNode.parentNode.parentNode);
          $.each(
            parent.find('li a'),
            function () {
              $(this).removeClass('hover');
            }
          )
          active_cItemMenu.addClass('hover')
        },
        offContext: function (element, event) {
          active_cItemMenu.removeClass('hover');
        }
      }


    }
  ]
)
