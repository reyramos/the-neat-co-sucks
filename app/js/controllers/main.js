'use strict';

/**
 * # Application
 *
 * Core Application controller that includes functions used before we kickStart the Application
 * The functions store within this files live outside of the ngView and are used as global function
 */
angular.module('app').controller(
  'MainController', ['$rootScope', '$scope', '$timeout', '$log', 'filesFactory', 'utilities', '$window', 'loaderService', 'socket', 'appData'
    , function ($rootScope, $scope, $timeout, $log, filesFactory, utilities, $window, loaderService, socket, appData) {

      var logger = $log.getInstance('MainController');

      /**
       * Sets the drag option for the sidebar
       * @type {{id: string, minWidth: number, maxWidth: number, position: string, direction: string, right: string, offset: string}}
       */
      $scope.sidebarOptions = {
        id: "leftContainer",
        minWidth: 225,
        maxWidth: 350,
        position: 'right',
        direction: "vertical",
        right: "#content",
        offset: "#leftbar"
      }

      //get all the files from requested directory
      var directoryContent = filesFactory.getFiles() || [],
          enums = appData.enums();
      $scope.files = []
      /**
       * Load only a certain number of files into the array, to use
       * infinite-scroll capabilities
       */
      function initializeFiles() {
        $scope.files = [];

        //loading a limited number for infinite scroll
        if (directoryContent.length) {
          var length = directoryContent.length < 100 ? directoryContent.length : 100;
          for (var i = 0; i < length; i++)
            $scope.files.push(directoryContent[i])
        }
      }

      initializeFiles();

      $scope.addMoreFiles = function () {
        var last = $scope.files.length - 1;
        if (directoryContent.length > $scope.files.length) {
          for (var i = 1; i <= 50; i++) {
            $scope.files.push(directoryContent[last + i]);
          }
        }
      }

      /***************************************************************************************************
       * SET THE TABLE HEADER FOR THE FILE ITEMS IN EACH FOLDERS
       ***************************************************************************************************/

        //set the headers
      $scope.cHeader = [
        {name: "", class: "icon", when: "icon"},
        {name: "Status", class: "status center", when: "status"},
        //{name: "Vendor", class: "vendor"},
        {name: "Filename", class: "filename", when: "filename"},
        //{temp:"Category",class:"category"},
        //{name: "Date", class: "date"},
        //{temp:"Payment Type",class:"ptype"},
        //{name: "Sale Tax", class: "stax"},
        //{name: "Misc Tax", class: "mtax"},
        //{name: "Total Amount", class: "tamount"},
        //{name:"Tax Category",class:"tcategory"},
        //{name:"Reimbursable",class:"reimbursable"},
        //{name: "Notes", class: "notes"},
        {name: "Last modified", class: "lmodified", when: "lmodified"},
        {name: "File size", class: "fsize", when: "fsize"},
      ];


      /**
       *
       * @param e
       * @param file
       */
      $scope.updatePendingStatus = function (e, file) {
        logger.info('updatePendingStatus', e, file)

        if (e && e.stopPropagation) {
          e.stopPropagation();
        } else {
          e = window.event;
          e.cancelBubble = true;
        }

        socket.send(enums['UPDATE_STATUS'], {_id: String(file['db_data']._id.$id)}).then(function (data) {
          logger.info(enums['UPDATE_STATUS'], data);
          $(e.target).hide();
        })

      }
      /***************************************************************************************************
       * SET THE DETAILS OF THE FILE ITEMS, ADJUST THE VIEW OF THE TABS
       ***************************************************************************************************/

      $scope.fileDetails = {};
      //set the view of loader of iframe
      $scope.tabView = null;

      function resetTabView() {

        $('#detailsTab').addClass('active');
        $('#viewTab').removeClass('active').addClass('disabled');

        $scope.tabView = "detailsTab";
      }

      $scope.getReceiptView = function (view) {
        $scope.setTabView(view)
        $scope.saveChanges = false;


        var obj = $scope.fileDetails;

        $timeout(function () {
          $('#frameBox').append($('<div class="list-loader"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>'));
        }, 1)

        socket.send(enums['GET_FILE'], {file: obj.file}).then(function (data) {
          logger.info(enums['GET_FILE'], data)
          $('#frameBox').empty();

          var url = data.data['url'].replace(".html", "001.png"),
              img = new Image();

          img.onload = function () {

            $timeout(function(){
              $('#frameBox').append(img);
            },100)


          };

          img.src = url;

        })

      }

      $scope.setTabView = function (view) {
        var _view = $('#' + view),
            children = _view.parent().children();
        angular.forEach(children, function (value, key) {
          $(children[key]).removeClass('active');
        });

        _view.addClass('active');

        $scope.safeApply(function () {
          $scope.tabView = view;
        })

      }


      /***************************************************************************************************
       * EDIT AND SAVE RECEIPT INFORMATION FUNCTIONALITY
       ***************************************************************************************************/
      var ids = ['receiptDate', 'taxAmount', 'totalAmount', 'otherAmount', 'otherName'];

      $scope.editReceiptInfo = function () {
        $scope.saveChanges = true;
        angular.forEach(ids, function (value, key) {
          $("#" + value).prop("disabled", false).removeClass('readonly')
        })
      }


      $scope.saveReceiptInfo = function (e) {

        reset_edit_btn_receipt_info();
        var data = $scope.fileDetails.receiptInfo,
            file = $scope.fileDetails;

        if (typeof(data.otherName) !== "undefined") {
          data[data.otherName[0]] = [];
          data[data.otherName[0]].push(data.otherName[1]);

          delete data.otherName;
        }

        var params = {
          date: data.date,
          params: data
        }

        delete data.date;


        socket.send(enums['UPDATE_RECEIPT'], {
          _id: String(file['db_data']._id.$id),
          data: params
        }).then(function (data) {
          logger.info(enums['UPDATE_RECEIPT'], data);

          $('li.open > .content-row').find('.update-btn').hide();
        })

      }


      $scope.pressEnterKey = function (e) {
        if (e.keyCode == 13) {
          logger.info('pressEnterKey');
          $scope.saveReceiptInfo(e)
        }
      }


      function reset_edit_btn_receipt_info() {
        $scope.saveChanges = false;
        angular.forEach(ids, function (value, key) {
          $("#" + value).prop("disabled", true).addClass('readonly')
        })


      }

      /***************************************************************************************************
       *
       ***************************************************************************************************/
      $scope.onClickFileItem = function (event, file) {
        logger.info('onClickFileItem', file)
        if (file.db_data && file.db_data.category === 'rec') {
          openDetailView(file);
          resetTabView();
        }

        var _this = $(event.currentTarget),
            _children = _this.parent().children();

        _this.toggleClass('open');

        if (!_this.hasClass('open'))
          $('#content').removeClass('left-bar-open');

        for (var i = 0; i < _children.length; i++) {
          !function outer(i) {
            if (!$(_children[i]).is(event.currentTarget) && $(_children[i]).has(event.currentTarget).length === 0) {
              $(_children[i]).removeClass('open on-dbl-click')
            }
          }(i);
        }


        socket.send(enums['GET_FILE'], {file: file.file}).then(function (data) {
          $('#viewTab').removeClass('disabled')
        })

      }

      function openDetailView(file) {


        $('#content').addClass('left-bar-open');
        var receiptInfo = {
          date: file.db_data.regex_data.date
        }

        angular.forEach(file.db_data.regex_data.params, function (val, key) {
          switch (key) {
            case "subtotal":
            case "total":
              receiptInfo.total = val;
              break;
            case "tax":
              receiptInfo.tax = val;
              break;
            default:
              receiptInfo[key] = val;
              break;
          }
        })


        logger.info('openDetailView', file)

        $scope.safeApply(function () {
          reset_edit_btn_receipt_info();
          $scope.saveChanges = false;
          $scope.fileDetails = file;
          $scope.fileDetails.receiptInfo = receiptInfo;
        })

      }

      $scope.onDblClick = function (event, obj) {


        var fileEncode = utilities.encode(obj.file);
        logger.info('onDblClick', fileEncode)

        var _this = $(event.currentTarget);
        _this.parent().addClass('on-dbl-click');

        //$window.open($window.location.href + 'file/' + fileEncode, fileEncode);

        socket.send(enums['GET_FILE'], {file: obj.file}).then(function (data) {
          logger.info(enums['GET_FILE'], data)

          var height = Math.ceil(Number(data.data.size.h) * 4),
              params = [
                "width=" + Math.ceil((Number(data.data.size.w) + 30) * 3),
                "height=" + (height < 867 ? height : 867),
                "menubar=0",
                "status=0",
                "titlebar=0",
                "toolbar=0",
              ],
              file = utilities.encode(data.data['pdf_file']),
              url = data.data['url'].replace(".html", "001.png");

          $window.open(url, file, params.join(','));

        })

      }


      /**
       * Updates the file list with the new content base on the folder selected
       */
      $scope.$on('fileItemsComplete:initialize', function () {

        $scope.safeApply(function () {
          directoryContent = filesFactory.getFiles();
          initializeFiles();
        })
      })

      $rootScope.$on('fileItemsComplete:completed', function () {
        loaderService.hide();
      })

    }]
);
