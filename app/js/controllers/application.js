'use strict';

/**
 * # Application
 *
 * Core Application controller that includes functions used before we kickStart the Application
 * The functions store within this files live outside of the ngView and are used as global function
 */
angular.module('app').controller(
  'Application', ['$rootScope', '$scope', '$log', '$timeout', 'modalFactory', 'socket', function ($rootScope, $scope, $log, $timeout, modalFactory, socket) {

    var logger = $log.getInstance('ApplicationController');

    $scope.upload = function () {
      modalFactory.show({
        template: '/modal/dropZone.html'
      })
    }



    $scope.safeApply = function (fn) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };

    $scope.$on("$routeChangeSuccess", function (next, current) {


    })

  }]
)
