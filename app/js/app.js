'use strict';

angular.module('app', [
  'ngRoute'
  , 'ngResource'
  , 'ngSanitize'
  , 'ngCookies'
  , 'ngAnimate'
  , 'ngTouch'
  //, 'ui.bootstrap.tabs'
  , 'myModules'

])
/**
 * @ngdoc object
 * @name app.TEMPLATE_CACHE
 *
 * @description
 * Building custom template, using Angular templateCache functionality, eliminate
 * ajax request for templates
 *
 * When defining this within your application make sure to use the full path of the file
 * $templateCache.get('/cache/savedFilters/SavedParms_Wdw.html')
 *
 */
  .constant('TEMPLATE_CACHE', {
    path: 'cache/', //root path of directory
    templates: []
  })

/**
 * @ngdoc object
 * @name app.LANGUAGE
 *
 * @description
 * Gather all the supported language from JSON files
 *
 * When defining this within your application it will make sure to gather all supported language
 * into the application
 *
 */
  .constant('LANGUAGE', {
    path: 'i18n/', //root path of directory
    languages: [
      "en", "sp"
    ]
  })

/**
 * @ngdoc object
 * @name app.APP_ENV
 *
 * @description
 * Provide application enviroment setting and variables
 *
 */

  .factory('APP_ENV', ['APP_OVERRIDE', function (APP_OVERRIDE) {
    var settings = {}
    //in the build.xml, build number gets replaced with the version number
    var template = 'template',
        version = 'BUILDNUMBER';

    //if version is a number
    if (!isNaN(parseFloat(version))) {
      template = template + '-' + version;
    }

    settings.profile = 'development'; //run with the development profile (only effects logging setup by default)
    settings.ga = ''; //Google Analytics

    //LOOP THROUGH OVERRIDES FOR THE ENV SETTINGS
    angular.forEach(APP_OVERRIDE, function (value, key) {
      settings[key] = value
    });

    return settings;

  }])
/**
 * @ngdoc service
 * @name app.service:APP_OVERRIDE
 * @kind object
 *
 *
 * @description
 * # THIS IS TO REMAIN AN EMPTY FACTORY
 *
 * This is a placeholder for production enviroments, it is set to override APP_ENV while in development
 *
 */
  .factory('APP_OVERRIDE', function () {
    return {}
  })
