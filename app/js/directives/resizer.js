/**
 * Created by redroger on 11/5/14.
 */

angular.module('app').directive('resizer', function($document, $window) {
	return function(scope, ele, attrs) {
		var options = scope[attrs.resizer],
			dragEle = angular.element('<div>' ).addClass('drag '+options.position );

		ele.append(dragEle);

		dragEle.on('mousedown', function(event) {
			event.preventDefault();
			dragEle.addClass('mousedown')
			$document.on('mousemove', mousemove);
			$document.on('mouseup', mouseup);
		});

		function mousemove(event) {

			if (options.direction == 'vertical') {
				var x = event.pageX;
				if (options.maxWidth && x > options.maxWidth) {
					x = parseInt(options.maxWidth);
				}else if (options.minWidth && x < options.minWidth) {
					x = parseInt(options.minWidth);
				}
				$(ele).css({ width: x + 'px' });
				if(options.right){
					$(options.right).css({left: x + 'px'});
				}

			} else {
				// Handle horizontal resizer
				//var y = window.innerHeight - event.pageY;

				//$element.css({
				//				 bottom: y + 'px'
				//			 });

				//$($attrs.resizerTop).css({
				//							 bottom: (y + parseInt($attrs.resizerHeight)) + 'px'
				//						 });
				//$($attrs.resizerBottom).css({
				//								height: y + 'px'
				//							});
			}
		}

		function mouseup() {
			$document.unbind('mousemove', mousemove);
			$document.unbind('mouseup', mouseup);
			dragEle.removeClass('mousedown')
		}
	};
});