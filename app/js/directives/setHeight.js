/**
 * Created by redroger on 11/5/14.
 */


angular.module('app').directive(
	'setHeight',['$window','utilities',function($window, utilities){
		var directive =
		{
			link: function ( scope, element, attr ) {

				var offset = Number(attr.setHeight ) * -1,
					wh = $window.innerHeight;

				var resize = function(){
					offset = Number(attr.setHeight ) * -1,
						wh = $window.innerHeight;

					element.css({height:(wh+offset)+'px'});
				}


				resize()
				utilities.addEvent(window, "resize", resize);

			}
		};
		return directive;
	}])
