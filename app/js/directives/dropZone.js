/**
 * Created by redroger on 10/30/14.
 */


angular.module('app' )
    .directive('dropZone', ['wsFileTransfer','$templateCache', '$compile', '$log', '$timeout',
							function(wsFileTransfer, $templateCache, $compile ,$log, $timeout) {


								var directive = {
									link: function(scope, element, attr) {

										var hiddenFileInput,
											uploaderContainer,
											uploaderProgressBar,
											allowDirectoryUpload = true,
											ignoreHiddenFiles = true,
											uploading = [];

										function handleDragStart() {
											event.dataTransfer.effectAllowed = 'move';
											element.unbind('dragstart')
										}

										function DragHandleFileSelect() {
											event.stopPropagation();
											event.preventDefault();

											if( !event.dataTransfer )
											{
												return;
											}

											var files = event.dataTransfer.files; // FileList object.
											// files is a FileList of File objects. List some properties.
											if (files.length) {
												var items = event.dataTransfer.items;
												if (items && items.length && ((items[0].webkitGetAsEntry != null) || (items[0].getAsEntry != null))) {
													handleItems(items);
												} else {
													handleFiles(files);
												}
											}

											element.unbind('drop')
											return false;

										}

										function handleFiles(files) {
											var file, _results;
											_results = [];
											for (var i = 0; i < files.length; i++) {
												uploadFile(files[i], i)
											}
											return _results;
										};

										function handleItems(items) {

											var entry, item, _i, _len;
											for (_i = 0, _len = items.length; _i < _len; _i++) {
												! function outer(_i) {
													item = items[_i];
													if (item.webkitGetAsEntry != null) {
														entry = item.webkitGetAsEntry();
														if (entry.isFile) {
															uploadFile(item.getAsFile(), _i)
														} else if (entry.isDirectory && allowDirectoryUpload) {
															addDirectory(entry, entry.name);
														} else {
															$log.log('SORRY DIRECTORY NOT ALLOWED');
														}
													} else {
														uploadFile(item.getAsFile(), _i)
													}
												}(_i);
											}
											removeDragOver();
										}

										function addDirectory(entry, path) {
											var dirReader, entriesReader;
											dirReader = entry.createReader();
											entriesReader = function(entries) {
												var _i, _len;
												for (_i = 0, _len = entries.length; _i < _len; _i++) {
													! function outer(_i) {
														entry = entries[_i];
														if (entry.isFile) {
															entry.file(
																function(file) {
																	if (ignoreHiddenFiles && file.name.substring(0, 1) === '.') {
																		return;
																	}
																	file.fullPath = "" + path + "/" + file.name;
																	return uploadFile(file, _i);
																}
															);
														} else if (entry.isDirectory) {
															addDirectory(entry, "" + path + "/" + entry.name);
														}

													}(_i);
												}
											};
											return dirReader.readEntries(
												entriesReader,
												function(error) {
													return typeof console !== "undefined" && console !== null ? typeof console.log === "function" ? console.log(error) : void 0 : void 0;
												}
											);
										};

										function handleDragOver() {
											event.stopPropagation();
											event.preventDefault();
											element.addClass('hover');
											event.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.

										}

										function removeDragOver() {
											element.removeClass('hover');
										}

										function uploadFile(files, index) {

											if(!uploaderContainer){
												uploaderContainer = $('<div id="TotalUploadContainer"><div class="progress"><div class="progress-bar"></div></div></div>');
												uploaderProgressBar = $( uploaderContainer[0].children[0].children[0])
												$('body').append(uploaderContainer);

												var DropBox = $('#dropBox'),
													DropZone = $(DropBox[0].childNodes[0] );

												DropZone.removeClass('in');
												$('#hidden-drop-zone' ).detach()

												$timeout(function(){
													DropBox.remove();
												},300)
												DropBox.unbind('mouseup');
											}

											var transfer = new wsFileTransfer({
											    file: files,
											    index: index,
											    blockSize: 1024,
											    progress: function(data) {
													uploading[data.index] = data.progress

													var total = 0,percent = 0;
														for(var i =0;i<uploading.length;i++){

															!function outer(i) {
																total += uploading[i];
																percent = total / (i + 1);
																uploaderProgressBar.css({width:percent+'%'})
															}(i);
														}

												},
											    success: function(data) {
													console.log('success',data)
													var count = 0;
													for(var i =0;i<uploading.length;i++){
														if(uploading[i] === 100)
															count++;
													}
													if(count === uploading.length){
														$timeout(function(){
															uploaderContainer.detach();
															uploaderContainer = null;
														},1500)
													}
											    }
											});
										}

										function setupHiddenFileInput() {

											if(!hiddenFileInput){

												hiddenFileInput = $('<input id="hidden-drop-zone" type="file" multiple="multiple">')
													.attr("id", "hidden-drop-zone")
													.attr("type", "file")
													.attr("multiple", "multiple" )
													.css({ visibility:"hidden", position:"absolute", top:"0", left:"0", height:"0", width:"0"})

												$('body').append(hiddenFileInput);
											}


											hiddenFileInput.on(
												"change",
												function(evt) {
													var files = evt.target.files; // FileList object
													for (var i = 0, f; f = files[i]; i++) {
														! function outer(i) {
															uploadFile(files[i], i)
													}(i);

													}

													//create a new one for every click
													return setupHiddenFileInput();
												})

											element.unbind('click');
											return hiddenFileInput.trigger( "click" );

										};


										element.on('click', setupHiddenFileInput);

										// Check for the various File API support.
										if (window.File && window.FileReader && window.FileList && window.Blob) {
											// Setup event listeners.
											element.on('dragstart', handleDragStart);
											element.on('dragover dragenter', handleDragOver);
											element.on('dragend dragleave', removeDragOver);
											element.on('drop', DragHandleFileSelect);
										}

										scope.$on('wsFileTransfer', function(event, $message) {
											//console.log('wsFileTransfer.BROADCAST: ', $message)
										});

									}
								}

								return directive;

        }
    ])