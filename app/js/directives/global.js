angular.module('app')


/**
 * @ngdoc overview
 * @name ngDancik.directive:a
 * @restrict E
 *
 * @description
 * Stop all event.preventDefault() and event.stopPropagation() from anchor with
 * attributes ngClick and href == #
 *
 * @example
 * ```html
 * <a href='#' ng-click='someFuntion()' ></a>
 *```
 *
 */
	.directive('a', [function() {
		return {
			restrict: 'E',
			link: function(scope, elem, attrs) {
				if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {

					elem.on('click', function(e) {
						event.preventDefault();
						event.stopPropagation();
					});
				}
			}
		};
	}])
