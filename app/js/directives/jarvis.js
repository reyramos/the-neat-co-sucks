/**
 * Created by redroger on 11/12/14.
 */


angular.module('app')
  .run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('/dynamicJarvis/ul.jarvisMenu',
        '<li class="@@class@@">' +
        '<a href="#" ng-click="folderSelector(\'@@files@@\')">' +
        '	<i class="fa fa-lg fa-fw fa-folder"></i>' +
        '	<span class="menu-item-parent">@@name@@</span>' +
        '	<span data-cm-override="contextItem" data-cm-name="@@_id@@"></span>' +
        '</a>' +
        '</li>'
      );
    }
  ])
  .directive('dynamicJarvis',
  ['$compile', 'utilities', '$templateCache', '$parse', '$rootScope',
    function ($compile, utilities, $templateCache, $parse, $rootScope) {

      var directive = {
        restrict: 'A',
        link: function (scope, element, attr) {
          var parentWrapper = "<ul></ul>",
              template = $templateCache.get('/dynamicJarvis/ul.jarvisMenu'),
              listArray = [], listContainer;

          $parse('DynamicJarvisObjectParse').assign($rootScope, []);


          function init() {
            listArray = attr.hasOwnProperty('dynamicJarvis') ? scope[attr.dynamicJarvis] : [];
          }


          function loopArray(list) {
            var $parentWrapper = angular.element(parentWrapper);
            for (var i = 0; i < list.length; i++) {
              var string = template, strEle;
              for (var key in list[i]) {
                if (list[i].hasOwnProperty(key)) {

                  if (typeof (list[i][key]) === 'object' && string.indexOf("@@" + key + "@@") > -1) {
                    //TODO: something about object parsing
                    scope['DynamicJarvisObjectParse'][utilities.encode(list[i]['path'] + list[i]['name'])] = {
                      path: list[i]['path'] + list[i]['name'],
                      obj: list[i][key],
                      utf:utilities.encode(list[i]['path'] + list[i]['name'])
                    }

                    string = utilities.str_replace("@@" + key + "@@",utilities.encode(list[i]['path'] + list[i]['name']), string);
                  } else {
                    string = utilities.str_replace("@@" + key + "@@", String(list[i][key]), string);
                  }

                  strEle = angular.element(string);
                  if (key.toLowerCase().trim() === "folders" && list[i][key].length) {
                    var subString = loopArray(list[i][key]);
                    strEle.append(angular.element(subString))
                  }
                }
              }
              $parentWrapper.append(strEle);
            }
            return $parentWrapper;
          }


          scope.$on('directive:dynamicJarvis', function () {
            if (listContainer) {
              listContainer.remove()
            }
            init();
            listContainer = loopArray(listArray);
            //add directive and properties
            listContainer
              .attr("data-jarvis-menu", "")
              .attr("options", "jarvisMenu")

            $compile(listContainer)(scope)
            element.append(listContainer);
          })

          scope.$broadcast('directive:dynamicJarvis')
        }
      }

      return directive;

    }])
