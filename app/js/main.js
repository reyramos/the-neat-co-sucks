// Require JS  Config File
require({
    baseUrl: '/js/',
    noGlobal: true,
    paths: {
      'appConfig': 'configs/app'
      , 'angular': '../../lib/angular/angular'
      , 'angular-resource': '../../lib/angular-resource/index'
      , 'angular-route': '../../lib/angular-route/index'
      , 'angular-cookies': '../../lib/angular-cookies/index'
      , 'angular-sanitize': '../../lib/angular-sanitize/index'
      , 'angular-animate': '../../lib/angular-animate/index'
      , 'angular-touch': '../../lib/angular-touch/index'
      , 'jquery': '../../lib/jquery/dist/jquery'
      , 'notification': 'vendor/smartadmin-plugin/notification/SmartNotification'

      //http://angular-ui.github.io/bootstrap/
      //, 'ui.bootstrap.tabs': '../../lib/angular-ui/src/tabs/tabs'


      /* Defining myModules Module */
      , 'moment': 'myModules/lib/moment/moment'
      , 'sprintf': 'myModules/lib/sprintf/index'
      , 'ua-parser': 'myModules/lib/ua-parser/src/ua-parser'
      , 'myModules': 'myModules/src/index'
      , 'myModules.templateCache': 'myModules/src/templateCache'
      , 'myModules.logger': 'myModules/src/logger'
      , 'myModules.loader': 'myModules/src/loader'
      , 'myModules.client': 'myModules/src/client'
      , 'myModules.whenScrolled': 'myModules/src/whenScrolled'
      , 'myModules.contextMenu': 'myModules/src/contextMenu'
      , 'myModules.socket': 'myModules/src/socket'
      , 'myModules.utilities': 'myModules/src/utilities'
      , 'myModules.onRepeatComplete': 'myModules/src/onRepeatComplete'

    }
    , map: {
      '*': {'jquery': 'jquery'}, 'noconflict': {"jquery": "jquery"}
    }
    , shim: {
      'app': {
        'deps': [
          'angular'
          , 'angular-route'
          , 'angular-resource'
          , 'angular-sanitize'
          , 'angular-animate'
          , 'angular-cookies'
          , 'angular-touch'
          //, 'ui.bootstrap.tabs'
          , 'myModules'
        ]
      }
      , 'angular': {'deps': ['jquery']}
      , 'angular-route': {'deps': ['angular']}
      , 'angular-resource': {'deps': ['angular']}
      , 'angular-cookies': {'deps': ['angular']}
      , 'angular-sanitize': {'deps': ['angular']}
      , 'angular-animate': {'deps': ['angular']}
      , 'angular-touch': {'deps': ['angular']}
      //, 'ui.bootstrap.tabs': {'deps': ['angular']}
      , 'notification': {'deps': ['jquery', 'appConfig']}
      , 'appConfig': {'deps': ['jquery']}
      , 'jquery': {
        init: function ($) {
          return $.noConflict(true);
        },
        exports: 'jquery'
      }

      /*  myModules Module Dependencies */
      , 'myModules': {
        'deps': [
          'angular',
          'myModules.templateCache',
          'myModules.logger',
          'myModules.client',
          'myModules.loader',
          'myModules.contextMenu',
          'myModules.whenScrolled',
          'myModules.onRepeatComplete',
          'myModules.socket',
          'myModules.utilities',
        ]
      }
      , 'myModules.client': {'deps': ['angular', 'ua-parser']}
      , 'myModules.templateCache': {'deps': ['angular']}
      , 'myModules.logger': {'deps': ['angular', 'sprintf', 'moment']}
      , 'myModules.loader': {'deps': ['angular']}
      , 'myModules.whenScrolled': {'deps': ['angular']}
      , 'myModules.contextMenu': {'deps': ['angular']}
      , 'myModules.onRepeatComplete': {'deps': ['angular']}
      , 'myModules.socket': {'deps': ['angular']}
      , 'myModules.utilities': {'deps': ['angular']}
      /*  /myModules Module */


      , 'routes': {
        'deps': [
          'app'
        ]
      }
      , 'controllers/main': {
        'deps': [
          'app'
        ]
      }
      , 'controllers/application': {
        'deps': [
          'app'
        ]
      }
      , 'controllers/folders': {
        'deps': [
          'app'
          , 'notification'
          , 'factory/folder'
          , 'factory/modal'
          , 'factory/appData'
        ]
      }
      , 'template/information': {
        'deps': [
          'app'
          , 'factory/files'
        ]
      }
      , 'controllers/files': {
        'deps': [
          'app'
          , 'factory/files'
        ]
      }

      , 'factory/wsFileTransfer': {
        'deps': [
          'app'
        ]
      }
      , 'factory/folder': {
        'deps': [
          'app'
          , 'factory/appData'
        ]
      }
      , 'factory/files': {
        'deps': [
          'app'
          , 'factory/appData'
        ]
      }
      , 'factory/appData': {
        'deps': [
          'app'
        ]
      }
      , 'directives/jarvisMenu': {
        'deps': [
          'app', 'jquery'
        ]
      }
      , 'directives/dropZone': {
        'deps': [
          'app'
        ]
      }
      , 'factory/modal': {
        'deps': [
          'app'
        ]
      }
      , 'directives/global': {
        'deps': [
          'app'
        ]
      }
      , 'directives/setHeight': {
        'deps': [
          'app'
        ]
      }
      , 'directives/resizer': {
        'deps': [
          'app'
        ]
      }
      , 'directives/jarvis': {
        'deps': [
          'app'
        ]
      }
      , 'service/analytics': {
        'deps': [
          'app', 'ua-parser'
        ]
      }
    }
  }
  , [
    'require'
    , 'routes'
    , 'directives/jarvisMenu'
    , 'directives/dropZone'
    , 'factory/modal'
    , 'directives/setHeight'
    , 'directives/resizer'
    , 'directives/global'
    , 'controllers/application'
    , 'controllers/folders'
    , 'template/information'
    , 'controllers/files'
    , 'controllers/main'
    , 'factory/wsFileTransfer'
    , 'factory/appData'
    , 'factory/folder'
    , 'directives/jarvis'
    , 'service/analytics'


  ]
  , function (require) {
    return require(
      [
        'bootstrap'
      ]
    )
  }
);
