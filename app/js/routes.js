angular.module('app').config(
  ['$routeProvider', function ($routeProvider) {

    var initializeData = ['appData','loaderService', function (appData, loaderService) {

      loaderService.start()

      return appData.initApplication();
    }];

    // We use AngularJS dependency injection to fetch the route provider.
    // The route provider is used to setup our app's routes.

    // The config below simply says when you visit '/' it'll render
    // the views/main.html template controlled by the Main controller.

    // The otherwise method specifies what the app should do if it doesn't recognise
    // the route entered by a user. In this case, redirect to home.
    $routeProvider
      .when('/', {
        templateUrl: '/views/index.html',
        controller: 'MainController',
        resolve: {
          'initializeData': initializeData
        }
      })
      .when('/file/:fileEncode', {
        templateUrl: '/views/file.html',
        controller: 'filesController',
        resolve: {
          'initializeData': initializeData
        }
      })
      .when('/pdf/:fileEncode', {
        templateUrl: '/views/viewPDF.html',
        controller: 'filesController',
        resolve: {
          'initializeData': initializeData
        }
      })
      .otherwise({
        redirectTo: '/'
      });


    /**
     * ## HTML5 pushState support
     *
     * This enables urls to be routed with HTML5 pushState so they appear in a
     * '/someurl' format without a page refresh
     *
     * The server must support routing all urls to index.html as a catch-all for
     * this to function properly,
     *
     * The alternative is to disable this which reverts to '#!/someurl'
     * anchor-style urls.
     */
    //$locationProvider.html5Mode(true)

  }])
  .run(['$log', 'Logger', function ($log, Logger) {
    Logger.enhanceAngularLog($log);
  }]);
