/**
 * Created by redroger on 01/23/15.
 */

angular.module('app')
  .factory('filesFactory', [
    'socket'
    , 'utilities'
    , '$log'
    , '$q'
    , 'appData'
    , '$rootScope'
    , function (socket
      , utilities
      , $log
      , $q
      , appData
      , $rootScope) {

      var logger = $log.getInstance('filesFactory'),
          enums = appData.enums(),
          service = {}, files = appData[enums.GET_CONTENT].data.files || [];

      service.call = function (dataType, data) {
        //var defer = $q.defer();
        //socket.send(dataType, data).then(function (data) {
        //  if (data.code === 200) {
        //    defer.resolve(data);
        //  } else {
        //    defer.reject(data)
        //  }
        //})
        //return defer.promise;
      }

      service.setFiles = function (arr) {
        files = arr;
        //call other jobs to process once we update the files
        $rootScope.$broadcast('fileItemsComplete:initialize')

      }

      service.getFiles = function () {
        return files;
      }

      return service;
    }]);
