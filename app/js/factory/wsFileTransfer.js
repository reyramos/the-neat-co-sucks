/**
 * Created by redroger on 10/30/14.
 */

angular.module('app')
    .factory('wsFileTransfer', [
        'socket',
        '$log',
        '$rootScope',
        function(
            socket,
            $log,
            $rootScope
        ) {

            // private static
            var DEFAULTS = {
                type: 'binary',
                binaryType: 'blob',
                blockSize: '1024'
            }

            // constructor
            var cls = function(options) {
                // private
                var options = angular.extend({}, DEFAULTS, options),
                    startTime, curIndex, lastBlock, filename = null;

                var response = {
                        'requestType': 'init',
                        'filename': options.file.name,
                        'size': options.file.size,
                        'type': options.type,
                        'parameters': [],
                        'index': options.index,
                        'promise': 'wsFileTransfer:' + options.index
                    },
                    _id;



                socket.send('wsFileTransfer', response).then(function(response) {
                    if (typeof(response._id) !== "undefined")
                        _id = response._id;
                    socketResponse(response)
                })

                var socketResponse = function(response) {
                    var self = this,
                        data = {};


                    //TODO:: do something for errors
                    if (response.code != 200) {
                        $log.error('SOMETHING WENT WRONG');
                        var error = {
                            message: response.message,
                            code: response.code
                        }
                        return false;
                    }

                    switch (response.type) {
                        case 'STOR':

                            // Response to a STOR command.
                            startTime = (new Date().getTime()) / 1000.0;
                            readSlice(0, options.blockSize);

                            break;
                        case 'DATA':

                            filename = response.filename

                            // Response to a DATA command.
                            // Copy object information into local var to have the
                            data = {
                                progress: curIndex / options.file.size * 100,
								index:response.index
                            }

                            //Send an asynchrone event to notify that some data has been sent
                            setTimeout(
                                function() {
                                    if (typeof(options.progress) == 'function') {
                                        options.progress(data);
                                    }
                                }, 0
                            );

                            // If all the data has been sent, send a success event
                            if (lastBlock) {
                                setTimeout(function() {
                                    if (typeof(options.progress) == 'function') {
                                        options.progress({progress: 100, index:response.index});
                                    }
                                    if (typeof(options.success) == 'function') {
                                        options.success({index:response.index});
                                    }
                                }, 0);

                                response.requestType = 'complete'
                                response._id = _id;

                                socket.send('wsFileTransfer', response).then(function(response) {
                                    socketResponse(response)
                                })
                                return;
                            }

                            // Read and send the next block
                            readSlice(curIndex + options.blockSize, options.blockSize);
                            break;
                        case 'STAT':
                            console.log('STAT: ', response)
                                //console.log('wsFileTransfer', data)
                                //socket.send ( 'wsFileTransfer: ' + JSON.stringify ( data ) );
                            break;

                    }
                }

                var readSlice = function(start, length) {
                    var self = this,
                        debug = arguments[2] || false
                        // Updates the current index
                    curIndex = parseInt(start);
                    // Make sure we stop at end of file
                    var stop = Math.min(start + length - 1, options.file.size - 1);
                    var length = stop - start + 1;
                    // Save if it is the last block of data to send
                    lastBlock = (stop == options.file.size - 1);

                    // Get blob and check his size
                    var blob = options.file.slice(start, start + length);
                    // Creates the reader only for base 64 encoded data
                    var reader = new FileReader();
                    reader.onabort = function() {
                        $log.error('reader: abort')
                    };
                    reader.onerror = function(event) {
                        switch (event.target.error.code) {
                            case event.target.error.NOT_FOUND_ERR:
                                $log.error('File not found');
                                break;
                            case event.target.error.NOT_READABLE_ERR:
                                $log.error('File is not readable');
                                break;
                            case event.target.error.ABORT_ERR:
                                $log.error('File upload aborted');
                                break;
                            default:
                                $log.error('An error occurred reading the file.');
                        };
                    };
                    // If we use STOR, we need to check the readyState.
                    reader.onloadend = function(evt) {
                        //only send if we have data to send
                        if (length > 0 && evt.target.readyState == FileReader.DONE) { // DONE == 2
                            sendB64Slice(event.target.result);
                        }
                    };
                    reader.readAsBinaryString(blob);
                }

                var sendB64Slice = function(data) {

                    response.requestType = 'data'
                    response.data = window.btoa(data)
                    response._id = _id;

                    socket.send('wsFileTransfer', response).then(function(response) {
                        socketResponse(response)
                    })
                }


            };


            return cls;

        }
    ]);