/**
 * # appData
 *
 * Initializes the Application, gets the necessary enums from http:/json/enums file,
 * Builds the appData object
 *
 */

angular.module('app').factory
('appData', [
    '$q'
    , '$rootScope'
    , '$log'
    , '$http'
    , 'socket'
    , 'clientService'
    , 'loaderService'
    , 'utilities'
    , function ($q
      , $rootScope
      , $log
      , $http
      , socket
      , clientService
      , loaderService
      , utilities) {

      var logger = $log.getInstance('appData');
      logger.info('clientService', clientService)

      var appData = {}
        , enums = null
        , requestTypes = []
        , count = 0;

      appData.enums = function () {
        return appData.application['RequestTypes']
      }

      /**
       * Get the necessary files and enums from server
       *
       * @returns {promise|a.fn.promise}
       */
      appData.initApplication = function () {
        var defer = $q.defer();

        socket.on().then(function () {
          //get the application configs settings
          $http.get('configs/application.json').success(function (data) {
            appData['application'] = data;
            getAllRequestConfigs().then(function () {

              logger.info('getAllRequestConfigs', appData)
              enums = appData.application['RequestTypes'];
              requestTypes.push(enums.GET_CONTENT);

              getRequest(count);
              var counting = $rootScope.$watch(
                function () {
                  return count === requestTypes.length ? true : false;
                }, function (status) {
                  if (!status) return;
                  if (count === requestTypes.length) {
                    loaderService.hide()
                    defer.resolve();
                  }
                  //stop the watch
                  counting();
                })
            });
          });
        })


        return defer.promise;
      }


      function getAllRequestConfigs() {
        var defer = $q.defer()
          , app = appData.application
          , handler = [];

        angular.forEach(app.requestConfigs, function (value, key) {
          this.push($http.get('configs/' + value + '.json'));
        }, handler);

        $q.all(handler).then(function (success) {
          angular.forEach(success, function (value, key) {
            var basename = utilities.basename(value.config.url, '.json')
            app[basename] = value.data;
          });

          defer.resolve();

        }, function (error) {
          logger.warn('error', error)

        })

        return defer.promise;
      }

      appData.update = function (key) {
        var defer = $q.defer();
        socket.send(key, {}).then(function (data) {
          appData[key] = data.code === 200 ? data.data : data;
          if (data.code === 200) {
            defer.resolve(data.data);
          } else {
            data.reject(data);
          }
        })
        return defer.promise;
      }

      function getRequest(index) {
        socket.send(requestTypes[index], {}).then(function (data) {
          appData[requestTypes[index]] = data.code === 200 ? data.data : data;
          count++;
          if (count < requestTypes.length)
            getRequest(count)
        }, function (resolve) {
          logger.warn('getRequest', requestTypes[index], resolve)
        })
      }

      appData.getError = function (name, code) {
        return appData.application[name][code];
      }

      return appData;


    }]
)
