/**
 * Created by redroger on 11/7/14.
 */

angular.module('app')
	.run([
			 '$templateCache',
			 function($templateCache) {
				 $templateCache.put('/modal/dropZone.html',
									'<div class="drop-box">' +
									'<div class="drop" data-drop-zone>' +
									'<div class="image-drop-upload">' +
									'<i class="fa fa-cloud-upload"></i>' +
									'</div>' +
									'<div class="image-drop-text">Drop files here or Click</div>' +
									'</div></div>'
				 )
			 }
		 ])
	.run([
			 '$templateCache',
			 function($templateCache) {
				 $templateCache.put('/modal/modal.html',
									'<div class="modal-box">' +
									'	<div class="panel {{modal.panel}}">' +
									'		<div class="panel-heading">' +
									'			<h3 class="panel-title" data-ng-bind-html="modal.title"></h3>' +
									'		</div>' +
									'	<div class="panel-body" data-ng-bind-html="modal.body"></div>' +
									'	</div>' +
									'</div>'
				 )
			 }
		 ])

	.directive('goModal', ['$templateCache','$timeout','$compile','$log',
						   function($templateCache,$timeout,$compile ,$log) {
							   return {
								   restrict: 'A',
								   link: function postLink(scope, element,attr) {
									   var opts = attr.hasOwnProperty('goModal')?scope[attr.goModal]:{speed:300,view:'/modal/modal.html'},
										   template = angular.element($templateCache.get(opts.template) ),
										   backDrop = angular.element('<div id="modalFactory" class="mf-backdrop"></div>' ).append(template)


									   if(!backDrop.length){
										   return;
									   }

									   var container = $(backDrop[0].childNodes[0] ),
										   ignoreHiddenFiles = true;

									   backDrop.on('mouseup',mouseUp);
									   $('body').append(backDrop)
									   $compile(backDrop )(scope);

									   $timeout(function(){
										   container.addClass('in')
									   },1)

									   function mouseUp (e) {
										   if (!container.is(e.target) && container.has(e.target).length === 0){
											   removeElement();
										   }
									   }

									   function removeElement(){
										   container.removeClass('in');
										   $('#hidden-drop-zone' ).detach()

										   $timeout(function(){
											   backDrop.detach();
										   },opts.speed)
										   backDrop.unbind('mouseup');
									   }


									   scope.$on('factory:modalFactory:hide',removeElement);
								   }
							   };
						   }
			   ] )

	.factory('modalFactory',['$document', '$animate', '$rootScope', '$compile','$timeout','$templateCache','$parse',
							 function($document,  $animate, $rootScope, $compile, $timeout, $templateCache, $parse){

				 var $parent = $document.find('body' ),
					 modalContainer,
					 backDrop,
					 options = {speed:300,template:'modal'};

				 return {

					 show:function(obj){

						 if(Object.keys(obj).length === 0)
						 	return;

						 obj = angular.extend({},options, obj)
						 $parse('goModalOptions').assign($rootScope, obj);

						 if(obj.hasOwnProperty('modal')){
							 $parse('modal').assign($rootScope, obj.modal);
						 }

						 backDrop = angular.element($templateCache.get(obj.template));
						 modalContainer = angular.element("<div>").attr("data-go-modal","goModalOptions")
						 $compile(modalContainer)($rootScope);
						 $rootScope.$broadcast('factory:modalFactory:show');
						 $animate.enter(modalContainer, $parent);
						 $timeout(function() {
							 $animate.leave(modalContainer);
						 },1);
					 },

					 hide:function(e){

						 $('#modalFactory').trigger('mouseup');
						 $rootScope.$broadcast('factory:modalFactory:hide');

					 }

				 }

			 }])
