/**
 * Created by redroger on 11/13/14.
 */

angular.module('app')
  .factory('folderFactory', [
    'socket'
    , '$log'
    , '$q'
    , 'appData'
    , function (socket
      , $log
      , $q
      , appData) {

      var logger = $log.getInstance('folderFactory'),
          enums = appData.enums(),
          folder = {};

      folder.call = function (dataType, data) {
        var defer = $q.defer();
        socket.send(dataType, data).then(function (data) {
          if (data.code === 200) {
            defer.resolve(data);
          } else {
            defer.reject(data)
          }
        })
        return defer.promise;
      }


      folder.getFolders = function () {

        logger.info(enums.GET_CONTENT, appData[enums.GET_CONTENT])

        var folders = appData[enums.GET_CONTENT].data.folders;


        return folders;
      }


      return folder;
    }]);
