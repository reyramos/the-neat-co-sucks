/**
 * Similar to JQuery.extend function
 * @param source
 * @returns {Object}
 */
Object.prototype.extend = function( source )
{
	for( var key in source ){
		if( source.hasOwnProperty ( key ) && !(key in this) ){
			this[key] = source[key];
		}
	}
	return this;
};

var wsFileTransfer = (function()
{
	// private static
	var DEFAULTS = {
			type:'binary', binaryType:'blob', blockSize:'1024'
		}
	// constructor
	var cls = function( options )
	{
		// private
		var options = options.extend ( DEFAULTS ), startTime, curIndex, lastBlock, response;
		var socket = new WebSocket ( options.url );
		var filename = null;


		socket.onopen = function( event )
		{

			var data = {
				'requestType':'init', 'filename':options.file.name, 'size':options.file.size, 'type':options.type, 'parameters':[]
			};
			this.send ( 'wsFileTransfer: ' + JSON.stringify ( data ) );
		}
		socket.onmessage = function( event )
		{
			var self = this,data = {};
			response = JSON.parse ( event.data );

			//TODO:: do something for errors
			if( response.code != 200 ){
				console.error('SOMETHING WENT WRONG');
				var error = {
					message:response.message, code:response.code
				}
				return false;
			}
			switch( response.type ){
				case 'STOR':

						// Response to a STOR command.
						startTime = (new Date ().getTime ()) / 1000.0;
						readSlice ( 0, options.blockSize );

					break;
				case 'DATA':

					filename = response.filename

					// Response to a DATA command.
					// Copy object information into local var to have the
					data = {
						progress : curIndex / options.file.size * 100
					}

					//Send an asynchrone event to notify that some data has been sent
					setTimeout (
						function()
						{
							if( typeof(options.progress) == 'function' ){
								options.progress ( data );
							}
						}, 0
					);

					// If all the data has been sent, send a success event
					if( lastBlock ) {
						setTimeout(function() {
							if( typeof(options.progress) == 'function' ){
								options.progress ( {progress:100} );
							}
							if( typeof(options.success) == 'function' ) {
								options.success(this);
							}
						}, 0);

						data = {
							'requestType':'complete', 'filename':filename
						};
						socket.send ( 'wsFileTransfer: ' + JSON.stringify ( data ) );
						return;
					}

					// Read and send the next block
					readSlice ( curIndex + options.blockSize, options.blockSize );
					break;
				case 'STAT':
					console.log('STAT: ',response)
					//console.log('wsFileTransfer', data)
					//socket.send ( 'wsFileTransfer: ' + JSON.stringify ( data ) );
					break;
				case 'COMP':
					socket.close();
					break;

			}
		}
		socket.onerror = function( event )
		{
			//self.onError ( event );
		}
		socket.onclose = function( event )
		{
			//self.onClose ();
		}
		var readSlice = function( start, length )
		{
			var self = this, debug = arguments[2] || false
			// Updates the current index
			curIndex = parseInt ( start );
			// Make sure we stop at end of file
			var stop = Math.min ( start + length - 1, options.file.size - 1 );
			var length = stop - start + 1;
			// Save if it is the last block of data to send
			lastBlock = (stop == options.file.size - 1);

			// Get blob and check his size
			var blob = options.file.slice ( start, start + length );
			// Creates the reader only for base 64 encoded data
			var reader = new FileReader ();
			reader.onabort = function()
			{
				console.error ( 'reader: abort' )
			};
			reader.onerror = function( event )
			{
				switch( event.target.error.code ){
					case event.target.error.NOT_FOUND_ERR:
						console.error ( 'File not found' );
						break;
					case event.target.error.NOT_READABLE_ERR:
						console.error ( 'File is not readable' );
						break;
					case event.target.error.ABORT_ERR:
						console.error ( 'File upload aborted' );
						break;
					default:
						console.error ( 'An error occurred reading the file.' );
				}
				;
			};
			// If we use STOR, we need to check the readyState.
			reader.onloadend = function( evt )
			{
				//only send if we have data to send
				if( length > 0 && evt.target.readyState == FileReader.DONE ){ // DONE == 2
					sendB64Slice ( event.target.result );
				}
			};
			reader.readAsBinaryString ( blob );
		}

		var sendB64Slice = function( data )
		{
			var data = {
				'requestType':'data', 'filename':filename, 'size':options.file.size, 'data':window.btoa ( data )
			};
			socket.send ( 'wsFileTransfer: ' + JSON.stringify ( data ) );
		}
		// public (this instance only) this
	};
	// public static
	cls.get_nextId = function()
	{
		return nextId;
	};
	// public (shared across instances)
	cls.prototype = {};
	return cls;
}) ();









