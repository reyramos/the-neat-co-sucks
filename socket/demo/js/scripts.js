function Ctrl ( $scope, $q )
	{
		var socket, websocket = function()
		{
			var socket = io.connect ( 'http://' + document.domain + ':9998' );
			socket.on (
				'welcome', function( data )
				{
					console.log ( data );
				}
			);
			socket.on (
				'connect', function( data )
				{
					console.log ( 'SOCKET_CONNECT' );
				}
			);
			socket.on (
				'message', function( message )
				{
					console.log ( message );
				}
			)
			return socket
		}
		socket = websocket ();
		function append ( string )
			{
				$scope.string += "\r\n" + string
			}

		function send ( eventType, data )
			{
				var callback = typeof (arguments[0]) == 'object' ? 'noEventType' : eventType;
				if( typeof (arguments[0]) == 'object' ){
					data = arguments[0]
					data.promise = callback;
					socket.send ( JSON.stringify ( data ) )
				}else{
					//if data is empty
					data = typeof (arguments[1]) == "undefined" ? {} : data
					data.promise = callback;
					var msg = typeof(data) == "object" ? JSON.stringify ( data ) : data;
					socket.emit ( eventType, msg )
				}
				console.debug ( 'socket.emit(' + callback + ')', data );
			}

		$scope.val = "";
		$scope.string = ""
		$scope.text = {}
		$scope.submit = function()
		{
			$scope.string = JSON.stringify ( $scope.val )
			var data;
			if( $scope.val[0] != '{' && $scope.val[$scope.val.length - 1] != '}' ){
				data = {data:$scope.val};
			}else{
				try{
					data = JSON.parse ( $scope.val );
				}catch( e ){
					console.log ( e )
				}
			}
			return $scope.eventType ? send ( $scope.eventType, data ) : send ( data )
		}
	}