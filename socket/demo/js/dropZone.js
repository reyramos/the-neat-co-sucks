/**
 * Created by redroger on 10/13/14.
 */
(function()
{
	var hiddenFileInput = document.getElementById ( 'hidden-drop-zone' ) || false,
		allowDirectoryUpload = true,
		ignoreHiddenFiles = true,
		dropZone = document.getElementById ( 'drop-zone' ),
		items = document.createElement ( 'div' );


	items.className = "items";
	dropZone.children[0].appendChild ( items );
	function DragHandleFileSelect ( evt )
		{
			evt.stopPropagation ();
			evt.preventDefault ();
			var files = evt.dataTransfer.files; // FileList object.
			// files is a FileList of File objects. List some properties.
			if( files.length ){
				var items = evt.dataTransfer.items;
				console.log ( 'items', items )
				if( items && items.length && ((items[0].webkitGetAsEntry != null) || (items[0].getAsEntry != null)) ){
					handleItems ( items );
				}else{
					handleFiles ( files );
				}
			}
		}
	function handleFiles ( files ) {
		var file, _results;
		_results = [];
		for( var i = 0; i < files.length; i++ )
		{
			addFile ( file )
		}
		return _results;
	};
	function handleItems ( items )
		{
			var entry, item, _i, _len;
			for( _i = 0, _len = items.length; _i < _len; _i++ ){
				item = items[_i];
				if( item.webkitGetAsEntry != null ){
					entry = item.webkitGetAsEntry ();
					if( entry.isFile ){
						addFile ( item.getAsFile () );
					}else if( entry.isDirectory && allowDirectoryUpload ){
						addDirectory ( entry, entry.name );
					}else{
						console.log ( 'SORRY DIRECTORY NOT ALLOWED' );
					}
				}else{
					addFile ( item.getAsFile () );
				}
			}
			removeDragOver ();
		}

	function addDirectory ( entry, path )
		{
			var dirReader, entriesReader;
			dirReader = entry.createReader ();
			entriesReader = function( entries )
			{
				var _i, _len;
				for( _i = 0, _len = entries.length; _i < _len; _i++ ){
					entry = entries[_i];
					if( entry.isFile ){
						entry.file (
							function( file )
							{
								if( ignoreHiddenFiles && file.name.substring ( 0, 1 ) === '.' ){
									return;
								}
								file.fullPath = "" + path + "/" + file.name;
								return addFile ( file );
							}
						);
					}else if( entry.isDirectory ){
						addDirectory ( entry, "" + path + "/" + entry.name );
					}
				}
			};
			return dirReader.readEntries (
				entriesReader, function( error )
				{
					return typeof console !== "undefined" && console !== null ? typeof console.log === "function" ? console.log ( error ) : void 0 : void 0;
				}
			);
		};
	function addFile ( file )
		{
			var reader = new FileReader ();
			// Closure to capture the file information.
			reader.onload = (function( theFile )
			{
				return function( e )
				{
					// Render thumbnail.
					var span = document.createElement ( 'span' );
					span.className = "inner-image";
					// Only process image files.
					if( file.type.match ( 'image.*' ) ){
						var image_path = e.target.result
						var imgClass = "thumb"
					}else{
						var image_path = "file.png";
						var imgClass = "document"
					}
					span.innerHTML = [
						'<img class="' + imgClass + '" src="',
						image_path,
						'" title="',
						escape ( theFile.name ),
						'"/>'
					].join ( '' );
					dropZone.children[0].children[0].appendChild ( span );
				};
			}) ( file );
			// Read in the image file as a data URL.
			reader.readAsDataURL ( file );

			uploadFile ( file )
		}

	function handleDragOver ( evt )
		{
			evt.stopPropagation ();
			evt.preventDefault ();
			dropZone.className = "hover";
			evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
		}

	function removeDragOver ()
		{
			dropZone.className = "";
		}

	function uploadFile ( files )
		{
			var $transfer = document.createElement ( "div" );
			$transfer.className = "transfer";
			var $progress = document.createElement ( "div" );
			$progress.className = "progress";
			var $progressBar = document.createElement ( "div" );
			$progressBar.className = "progressBar";
			$progressBar.appendChild ( $progress );
			$transfer.appendChild ( $progressBar );
			document.getElementById ( 'progresses' ).appendChild ( $transfer );

			console.log('socket',socket)

			//var transfer = new wsFileTransfer (
			//	{
			//		socket:socket, file:files, blockSize:1024, progress:function( data )
			//	{
			//		$progress.style.width = data.progress + '%';
			//	}, success:function( event )
			//	{
			//		$progress.className += " finished";
			//	}
			//	}
			//);
		}

	function setupHiddenFileInput ()
		{
			if( hiddenFileInput ){
				document.body.removeChild ( hiddenFileInput );
			}
			hiddenFileInput = document.createElement ( "input" );
			hiddenFileInput.setAttribute ( "id", "hidden-drop-zone" );
			hiddenFileInput.setAttribute ( "type", "file" );
			hiddenFileInput.setAttribute ( "multiple", "multiple" );
			hiddenFileInput.style.visibility = "hidden";
			hiddenFileInput.style.position = "absolute";
			hiddenFileInput.style.top = "0";
			hiddenFileInput.style.left = "0";
			hiddenFileInput.style.height = "0";
			hiddenFileInput.style.width = "0";
			document.body.appendChild ( hiddenFileInput );
			hiddenFileInput.addEventListener (
				"change", function( evt )
				{
					var files = evt.target.files; // FileList object
					for( var i = 0, f; f = files[i]; i++ ){
						!function outer ( i )
						{
							uploadFile ( files[i] )
						} ( i );
					}
					//create a new one for every click
					return setupHiddenFileInput ();
				}, false
			);
			return hiddenFileInput.click ();
		};
	// Check for the various File API support.
	if( window.File && window.FileReader && window.FileList && window.Blob ){
		// Setup the dnd listeners.
		dropZone.addEventListener ( 'dragover', handleDragOver, false );
		dropZone.addEventListener ( 'dragleave', removeDragOver, false );
		dropZone.addEventListener ( 'dragend', removeDragOver, false );
		dropZone.addEventListener ( 'drop', DragHandleFileSelect, false );
		dropZone.addEventListener ( 'click', setupHiddenFileInput, false );
	}else{
		console.warn ( 'The File APIs are not fully supported in this browser.' );
	}
}) ()
