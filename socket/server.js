var fs    = require('fs'),
    php   = require('phpjs'),
    shell = require('shelljs'),
    php   = require('phpjs');

//create a JSON file to load the collection tables
shell.exec('php ' + require('path').dirname(__dirname) + '/configs/easy/json_configs.php', {silent: true});

//  ======================================================
//  Custom Libraries
//  ======================================================
var winston = require('./libs/winstonColors');
var nconf = require('./libs/nconfController');
var config = require('../configs/easy/socket/' + nconf.get('NODE_ENV'));
var mongo = require('./libs/mongoModel');
var enums = require('./libs/enums');
var collections = require('../configs/easy/socket/collections');
var debug = require('./libs/debug').DEBUG;


//  ======================================================
//  Needed Server,Restful libraries
//  ======================================================
var io = require('socket.io').listen(9998);


console.log("http://localhost:9998");
console.log("    ███████╗ █████╗ ███████╗██╗   ██╗");
console.log("    ██╔════╝██╔══██╗██╔════╝╚██╗ ██╔╝");
console.log("    █████╗  ███████║███████╗ ╚████╔╝ ");
console.log("    ██╔══╝  ██╔══██║╚════██║  ╚██╔╝  ");
console.log("    ███████╗██║  ██║███████║   ██║   ");
console.log("    ╚══════╝╚═╝  ╚═╝╚══════╝   ╚═╝   ");
console.log("                               Done!!");
winston.info('NODE_ENV: ' + nconf.get('NODE_ENV'));

//TODO:TODO_CREATE_UNIQUE_USER_ID
//array to store all clients connected
var clients = [],
    files   = [];

io.on('connection', function (socket) {

  var connection = {
    id: socket.id,
    socket: socket,
    transport: socket.transport,
    ip: getSourceIp(socket)
  }

  //push it to clients array
  clients[connection.id] = connection;
  winston.info('SOCKET ID: ' + socket.id);

  /**
   * Generic message listener
   */
  socket.on('message', function (message, callback) {
    winston.info("message :", message)
  })




  socket.on('updateReceiptInformation', function (message) {
    var data = JSON.parse(message);

    //initialize the collection to use
    mongo.setCollection(collections.files.name)
    mongo.update(data._id, {regex_data:data.data, status: "APPROVED"}).then(function (result) {
      var response = {
        code: 200,
        eventType: data.eventType,
        promise: data.promise
      };

      socket.send(response);

    }, function (error) {

      var err = {
        broadcast: "updateReceiptInformation",
        message: error,
        code: 500
      };

      socket.send(err);

    });

  })



  /**
   * Update the status of the receipt data.
   * Set the status to 'APPROVED'
   */
  socket.on('updateStatus', function (message) {
    var data = JSON.parse(message);

    //initialize the collection to use
    mongo.setCollection(collections.files.name)
    mongo.update(data._id, {
      "status": "APPROVED"
    }).then(function (result) {
      var response = {
        code: 200,
        eventType: data.eventType,
        promise: data.promise
      };

      socket.send(response);

    }, function (error) {

      var err = {
        broadcast: "updateStatus",
        message: error,
        code: 500
      };

      socket.send(err);

    });


  })


  socket.on('getFile', function (message) {
    var data = JSON.parse(message),
        response = {
          "promise": data.promise,
          "eventType": data.eventType
        };
    var command = 'php ' + config.appPath + '/scripts/getFile.php --file "' + data.file + '"';

    shell.exec(command, {
      silent: true
    }, function (code, output) {

      response.data = JSON.parse(output);
      socket.send(response);

    });

  })

  /********************************************************
   * UI CALL FOLDERS EVENTS
   * *****************************************************/
  socket.on('getContent', function (message) {
    var data = JSON.parse(message),
        response = {
          "promise": data.promise,
          "eventType": data.eventType
        };
    var command = 'php ' + config.appPath + '/scripts/getDirContents.php --uid ' + data.user;
    shell.exec(command, {
      silent: true
    }, function (code, output) {

      response.data = JSON.parse(output);
      socket.send(response);

    });

  })

  socket.on('uploadContent', function (message) {
    var data = JSON.parse(message),
        response = {
          "promise": data.promise,
          "eventType": data.eventType
        };

    socket.send(response);

  })

  /********************************************************
   * UI CALL FILE TRANSFER EVENTS
   * *****************************************************/

  /**
   * Special message listener for wsFileTranfer client,
   * It depends on a callback from the frontend while the file is being uploaded.
   * Once the file is uploaded it will depend on the server to complete the process
   * while broadcasting message to client, on the status of the progress
   *
   */
  socket.on('wsFileTransfer', function (message) {

    var data = JSON.parse(message),
        response = {};


    console.log("STOP", data);
    return;

    switch (data.requestType) {
      case "init":

        //initialiaze the collection to use
        mongo.setCollection(collections.uploads.name)

        //the data to insert into the database
        var insert = {
          "user_id": "TODO_CREATE_UNIQUE_USER_ID",
          "status": enums.INITIALIZE,
          "path": config.uploadDir,
          "date_time": new Date(),
          "filename": data.filename,
          "size": data.size,
          "type": String(data.filename).substr(String(data.filename).lastIndexOf('.') + 1)
        };

        mongo.put(insert).then(function (id) {

          files[data.index] = {
            filename: data.filename,
            promise: data.promise,
            size: data.size,
            wStream: fs.createWriteStream(config.uploadDir + '/' + id + '.pdf')
          };


          response = {
            _id: String(id),
            type: "STOR",
            message: "Upload initialized. Wait for data",
            code: 200,
            filename: data.filename,
            promise: data.promise,
            index: data.index
          };

          socket.send(response);
        }, function (error) {
          var err = {
            broadcast: "wsFileTransfer",
            message: error,
            code: 500,
            filename: data.filename,
            promise: data.promise,
            index: data.index
          };

          socket.send(err);


        })

        break;
      case "data":
        var buffer = new Buffer(data.data, 'base64');
        files[data.index].wStream.write(buffer);

        //initialiaze the collection to use
        mongo.setCollection(collections.uploads.name)
        mongo.update(data._id, {
          "status": enums.UPLOADING
        }).then(function (result) {
          response = {
            type: "DATA",
            bytesRead: buffer.length,
            code: 200,
            filename: data.filename,
            promise: data.promise,
            index: data.index
          };

          socket.send(response);

        }, function (error) {

          var err = {
            broadcast: "wsFileTransfer",
            message: error,
            code: 500,
            filename: data.filename,
            promise: data.promise,
            index: data.index
          };

          socket.send(err);

        });
        break;
      case "complete":

        files[data.index].wStream.end();
        delete files[data.index];


        mongo.setCollection(collections.uploads.name)
        mongo.update(data._id, {
          "status": enums.COMPLETED
        }).then(function (result) {
          response = {
            _id: String(id),
            type: "STAT",
            code: 200,
            filename: data.filename,
            request: "COMPLETED",
            promise: data.promise,
            index: data.index
          };

          socket.send(response);

        }, function (error) {

          var err = {
            broadcast: "wsFileTransfer",
            message: error,
            code: 500,
            filename: data.filename,
            promise: data.promise,
            index: data.index
          };

          socket.send(err);

        });

        if (debug === false) {

          var $count = 'php ' + config.appPath + '/rachet/reader.php COUNT ' + data._id + '.pdf';

          shell.exec($count, {
            silent: true,
            async: false
          }, function (code, output) {
            var response = JSON.parse(output);
            response.index = data.index;

            console.log($count)
            console.log(response)

            socket.send(response);

            for (var i = 1; i <= response.pages; i++) {

              !function outer(i) {
                var counter = JSON.parse(output);
                delete counter.request;
                counter.index = data.index;
                counter.status = "NEW_FILE";
                counter.page = i;

                socket.send(counter);


                var $new = 'php ' + config.appPath + '/rachet/reader.php NEW ' + data._id + '.pdf' + ' ' + i;
                shell.exec($new, {
                  silent: true
                }, function (code, output) {
                  var res = JSON.parse(output);
                  delete counter.request;
                  res.index = data.index;

                  console.log($new)
                  console.log(res)

                  socket.send(res);

                  var $read = 'php ' + config.appPath + '/rachet/reader.php READ ' + res.new_file_name;
                  shell.exec($read, {
                    silent: true
                  }, function (code, output) {
                    var read = JSON.parse(output);
                    delete read.request;
                    read.index = data.index;

                    console.log($read)
                    console.log(read)

                    socket.send(read);

                  });
                });

              }(i);
            }
          });

        }


        break;
    }

  });

  /**
   * Drop the connection from the array, the user has disconnected
   * by dropping page, or network disconnect
   * Drop connection from clients array,
   * Check if client also started to play to drop player
   *
   */
  socket.on('disconnect', function () {
    winston.info('user disconnected >> ' + connection.id)
    delete clients[connection.id];
  });
});


/**
 * Collect the Clients connection IP address
 * @param conn
 */
function getSourceIp(conn) {
  if (
    typeof conn.headers !== 'undefined' && typeof conn.headers['x-forwarded-for'] !== 'undefined' && conn.headers['x-forwarded-for'] !== ''
  ) {
    conn._sourceIp = conn.headers['x-forwarded-for'];
    conn._sourceIpVar = 'xff';
  } else if (
    typeof conn.remoteAddress !== 'undefined' && conn.remoteAddress !== ''
  ) {
    conn._sourceIp = conn.remoteAddress;
    conn._sourceIpVar = 'ra';
  }
}
