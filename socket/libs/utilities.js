/**
 * Created by redroger on 4/15/14.
 */

var service = {}
service.objectKeyExists = function( value , obj) {

	if( obj === null || typeof(obj) == 'undefined' ) {
		return false;
	}
	var results = false;
	Object.keys(obj).forEach(function(key) {

		if(key.toLowerCase() ===  value.trim().toLowerCase()){
			results = true;
		}

	});

	return results;
}

service.arrayUnique = function(array ) {
	var a = array.concat();
	for( var i=0; i < a.length; ++i ) {
		for( var j = i+1; j < a.length; ++j ) {
			if( a[i] === a[j] ) {
				a.splice(j--, 1);
			}
		}
	}
	return a;
}

service.arrayIntersection = function( x, y ) {
	var ret = [];
	for ( var i = 0; i < x.length; i++ ) {
		for ( var z = 0; z < y.length; z++ ) {
			if ( x[i] == y[z] ) {
				ret.push(i);
				break;
			}
		}
	}
	return ret;
}

service.contains = function(value, array) {
	var i = array.length;
	while (i--) {
		if (array[i].trim().toLowerCase() === value.trim().toLowerCase()) {
			return true;
		}
	}
	return false;;
}

service.isEmpty = function( object ) {
	// null and undefined are empty
	if( object == null ) {
		return true;
	}
	// Assume if it has a length property with a non-zero value
	// that that property is correct.
	if( object.length && object.length > 0 ) {
		return false;
	}
	if( object.length === 0 ) {
		return true;
	}
	for( var key in object ) {
		if( hasOwnProperty.call( object, key ) ) {
			return false;
		}
	}
	// Doesn't handle toString and toValue enumeration bugs in IE < 9
	return true;
}

module.exports = service;