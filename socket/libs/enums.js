module.exports= {
	PENDING:"PENDING",
	INITIALIZE:"INITIALIZE",
	READING:"READING",
	COMPLETED:"COMPLETED",
	FAILED:"FAILED",
	PAUSED:"PAUSED",
	NEW_PAGE:"NEW_PAGE",
	UPLOADING:"UPLOADING",
	UPLOAD_COMPLETE:"UPLOAD_COMPLETE",
	NEW_FILE:"NEW_FILE",
	READ_FILE:"READ_FILE",
	COUNT:"COUNT",
	COUNTING:"COUNTING"

}
