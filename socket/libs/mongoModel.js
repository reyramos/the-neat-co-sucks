/**
 * Created by redroger on 11/02/14.
 */
var winston = require('./winstonColors');
var nconf = require('./nconfController');
var enums = require('./enums');
var debug = require('./debug').DEBUG;
var q = require('q');
var mongo = require('mongodb');

//  ======================================================
//  Variables and Config :: for server only
//  ======================================================


var config = require('../../configs/easy/socket/' + nconf.get('NODE_ENV'))['db'];
var collections = require('../../configs/easy/socket/collections');


//  ======================================================
//  mongodb, redis connection
//  ======================================================
var MongoClient = mongo.MongoClient,
    assert = require('assert'),
    ObjectId = mongo.ObjectID;




//EX: WITH AUTHENTICATION
var database = [];

// Use connect method to connect to the Server
MongoClient.connect(config.mongo_uri_auth,
    function(err, db) {
        assert.equal(null, err);
        winston.info("Binding the mongodb collection");
        Object.keys(collections).forEach(function(key) {
            winston.info(collections[key].name)
            database[collections[key].name] = db.collection(collections[key].name);
        });

    });

var service = {
    collection: null,
    dbCollection: ""
}

service.setCollection = function(collection) {
    service.collection = database[collection];
    service.dbCollection = collection;
}


service.get = function(query) {

    var deferred = q.defer();

    service.collection.find(query).toArray(function(err, array) {
        if (err) {
            winston.error(err)
            deferred.reject(err);
        } else if (array) {
            deferred.resolve(array.length ? array : false)
        }
    });

    return deferred.promise;

}

service.getById = function(id, query) {

    var deferred = q.defer(),
        search = {
            _id: new ObjectId(id)
        };

    for (var key in query) {
        if (query.hasOwnProperty(key)) {
            search[key] = query[key];
        }
    }

    service.collection.find(search).toArray(function(err, array) {
        if (err) {
            winston.error(err)
            deferred.reject(err);
        } else if (array) {
            deferred.resolve(array.length ? array : false)
        }
    });

    return deferred.promise;

}


/**
 * Insert data into Mongo, it will check with configs for indexing
 * @param query
 * @returns q.defer().promise(id) return the id of the mondo id
 *
 */
service.put = function(query) {
    var deferred = q.defer();


    if (debug) {
        deferred.resolve(123456789);
    } else {
        createIndexing();
        service.collection.insert(query, function(err, result) {
            if (err) {
                deferred.reject(err);
            } else if (result) {
                deferred.resolve(result[0]._id);
            }

        });
    }



    return deferred.promise;

}

/**
 * update the collection by ID
 *
 * @param {string} mongoId
 * @param {object} update data
 */
service.update = function(id, query) {
    var deferred = q.defer();
    if (debug) {
        deferred.resolve(true);
    } else {

        service.collection.update({
            _id: {
                '$in': [new ObjectId(id)]
            }
        }, {
            '$set': query
        }, function(err, result) {
            if (err) {
                deferred.reject(err);
            } else if (result) {
                deferred.resolve(result);
            }

        });
    }


    return deferred.promise;

};


/**
 * Create Indexing
 */
function createIndexing() {
    if (typeof(collections[service.dbCollection].ensureIndex) !== "undefined") {
        var indexing = collections[service.dbCollection].ensureIndex,
            keys = {},
            options = {};

        for (var i in indexing.key) {
            keys[indexing.key[i]] = 1
        }
        for (var i in indexing.type) {
            options[indexing.type[i]] = true
        }
        service.collection.ensureIndex(keys, options, function(err, result) {
            if (err) {
                winston.error(err);
                throw err;
            }
        })
    }
}

module.exports = service;
