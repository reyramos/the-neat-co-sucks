<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 12/28/2014
 * Time: 8:05 AM
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

$keywords = include_once(dirname(__DIR__) . "/configs/easy/regex/search.php");


$dir = "tests/text";
$files = scandir($dir);


function keywordSort($a, $b)
{
  return strlen($b) - strlen($a);
}

uksort($keywords, "keywordSort");

$min = 0;
$count = 0;
foreach ($keywords as $key => $keyword) {
  $handler = $keyword;
  usort($handler, "keywordSort");
  $keywords[$key] = $handler;


  if ($count === 0) {
    $max = strlen($key);
  }elseif($count === (count($keywords) - 1)) {
    $min = strlen($key);
  }


  if (count($keywords[$key])){
    if(strlen($keywords[$key][0]) > $max) {
      $max = strlen($keywords[$key][0]);
    }elseif(strlen($keywords[$key][count($keywords[$key]) - 1]) < $min) {
      $min =  strlen($keywords[$key][count($keywords[$key]) - 1]);
    }
  }

  $count++;
}




foreach ($files as $file) {

  $params = array();


  if ($file !== ".." && $file !== "." && !is_dir($dir . "/" . $file)) {
    $filename = $dir . "/" . $file;

    $str = strtolower(file_get_contents($filename));
    $lines = explode("\n", $str);

//    echo "<pre>" . print_r($lines, true) . "</pre>";

    for ($lineNumber = 0; $lineNumber < count($lines); $lineNumber++) {
      $line = strtolower($lines[$lineNumber]);

      include "REGEXs/date.php";
      include "REGEXs/params.php";

    }

    $receipts = array(
      "date" => isset($receipt_date) ? $receipt_date : "",
      "params" => $params
    );

    echo "<pre>" . print_r($receipts, true) . "</pre>";

    echo "<br> ===================================================== <br>";

  }
}
