#!/bin/bash
dir=$1
filename=$2

clear
soffice --headless --convert-to pdf --outdir "${dir}" "${filename}"