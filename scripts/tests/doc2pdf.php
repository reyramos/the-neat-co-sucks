<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 1/11/2015
 * Time: 7:30 PM
 *
 *
 * Convert from and to any format supported by LibreOffice

unoconv options:
-c, --connection=string  use a custom connection string
-d, --doctype=type       specify document type
(document, graphics, presentation, spreadsheet)
-e, --export=name=value  set export filter options
eg. -e PageRange=1-2
-f, --format=format      specify the output format
-i, --import=string      set import filter option string
eg. -i utf8
-l, --listener           start a permanent listener to use by unoconv clients
-n, --no-launch          fail if no listener is found (default: launch one)
-o, --output=name        output basename, filename or directory
--pipe=name          alternative method of connection using a pipe
-p, --port=port          specify the port (default: 2002)
to be used by client or listener
--password=string    provide a password to decrypt the document
-s, --server=server      specify the server address (default: 127.0.0.1)
to be used by client or listener
pgm      - Portable Graymap [.pgm]
png      - Portable Network Graphic [.png]
potm     - Microsoft PowerPoint 2007/2010 XML Template [.potm]
pot      - Microsoft PowerPoint 97/2000/XP Template [.pot]
ppm      - Portable Pixelmap [.ppm]
pptx     - Microsoft PowerPoint 2007/2010 XML [.pptx]
pps      - Microsoft PowerPoint 97/2000/XP (Autoplay) [.pps]
ppt      - Microsoft PowerPoint 97/2000/XP [.ppt]
pwp      - PlaceWare [.pwp]
ras      - Sun Raster Image [.ras]
sda      - StarDraw 5.0 (OpenOffice.org Impress) [.sda]
sdd      - StarImpress 5.0 [.sdd]
sdd3     - StarDraw 3.0 (OpenOffice.org Impress) [.sdd]
sdd4     - StarImpress 4.0 [.sdd]
sxd      - OpenOffice.org 1.0 Drawing (OpenOffice.org Impress) [.sxd]
sti      - OpenOffice.org 1.0 Presentation Template [.sti]
svg      - Scalable Vector Graphics [.svg]
svm      - StarView Metafile [.svm]
swf      - Macromedia Flash (SWF) [.swf]
sxi      - OpenOffice.org 1.0 Presentation [.sxi]
tiff     - Tagged Image File Format [.tiff]
uop      - Unified Office Format presentation [.uop]
vor      - StarImpress 5.0 Template [.vor]
vor3     - StarDraw 3.0 Template (OpenOffice.org Impress) [.vor]
vor4     - StarImpress 4.0 Template [.vor]
vor5     - StarDraw 5.0 Template (OpenOffice.org Impress) [.vor]
wmf      - Windows Metafile [.wmf]
xhtml    - XHTML [.xml]
xpm      - X PixMap [.xpm]

The following list of spreadsheet formats are currently available:

csv      - Text CSV [.csv]
dbf      - dBASE [.dbf]
dif      - Data Interchange Format [.dif]
fods     - OpenDocument Spreadsheet (Flat XML) [.fods]
html     - HTML Document (OpenOffice.org Calc) [.html]
ods      - ODF Spreadsheet [.ods]
ooxml    - Microsoft Excel 2003 XML [.xml]
ots      - ODF Spreadsheet Template [.ots]
pdf      - Portable Document Format [.pdf]
pxl      - Pocket Excel [.pxl]
sdc      - StarCalc 5.0 [.sdc]
sdc4     - StarCalc 4.0 [.sdc]
sdc3     - StarCalc 3.0 [.sdc]
slk      - SYLK [.slk]
stc      - OpenOffice.org 1.0 Spreadsheet Template [.stc]
sxc      - OpenOffice.org 1.0 Spreadsheet [.sxc]
uos      - Unified Office Format spreadsheet [.uos]
vor3     - StarCalc 3.0 Template [.vor]
vor4     - StarCalc 4.0 Template [.vor]
vor      - StarCalc 5.0 Template [.vor]
xhtml    - XHTML [.xhtml]
xls      - Microsoft Excel 97/2000/XP [.xls]
xls5     - Microsoft Excel 5.0 [.xls]
xls95    - Microsoft Excel 95 [.xls]
xlt      - Microsoft Excel 97/2000/XP Template [.xlt]
xlt5     - Microsoft Excel 5.0 Template [.xlt]
xlt95    - Microsoft Excel 95 Template [.xlt]
xlsx     - Microsoft Excel 2007/2010 XML [.xlsx]

 */


$command = 'doc2pdf  -o "/home/redroger/development/easy/" "/media/volume2/gDrive/Dancik/QUARTER GOALS/2015 Quarterly
 APR.xls"';
