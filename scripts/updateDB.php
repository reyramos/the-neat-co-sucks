<?php
  /**
   * Created by PhpStorm.
   * User: redroger
   * Date: 2/1/2015
   * Time: 5:28 PM
   */


  use Easy\Models\MongoConnect;
  use Easy\Enums\Collection;

  require dirname(__DIR__) . '/rachet/vendor/autoload.php';
  require dirname(__DIR__) . "/configs/easy/rachet/constants.php";


//lets finally load some stuff
  MongoConnect::configure(CONFIGS . '/environment.php');

  $mongo = new MongoConnect();
  $result = $mongo->MongoFetchAll(Collection::FILES);

  foreach ($result as $arr) {
    echo $arr['_id'] . "\n";
    $mongo->MongoUpdate(new \MongoId($arr['_id']), array(
      "category" => "rec"
    ), Collection::FILES);
  }



