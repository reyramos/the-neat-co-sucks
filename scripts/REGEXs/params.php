<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 1/1/2015
 * Time: 8:34 PM
 *
 * Gathers the parameters needed to pass into Easy\Controllers
 * Searches each line from given file with the regular expression text
 *
 * The parent File gives the following
 * $line - text line from file, reference
 * $min - min number to search by in the kyeword array
 * $max - max number character in the keyword array
 * $keyword - array of keywords to search for in each line
 *
 * @return array $params
 *
 */


$re = "/(\\w{" . $min . "," . $max . "}[\\]]?)[.:]?[\\s]*[\\$%]?(\\d+[.%]?[\\s]*\\d+[%]?)[\\D]*[\\s]*[\\$%]?(\\d+[.%]?[\\s]*\\d+[%]?)*[\\D]*[\\s]*([\\$%]?\\d+[.%\\s]?\\d+[%]?)*/";

preg_match($re, $line, $matches);

if (count($matches)) {
//  echo "<pre>".print_r($matches,true)."</pre>";

  foreach ($matches as $key => $param) {

    if (isset($keywords[trim($param)])) {
      $params = array_merge($params, array($param => array_unique($matches)));
      continue;
    } else {
      foreach ($keywords as $pkey => $keyword) {
        if (in_array(trim($param), $keyword)) {
          $params = array_merge($params, array($pkey => array_unique($matches)));
          continue;
        }
      }
    }
  }
}

