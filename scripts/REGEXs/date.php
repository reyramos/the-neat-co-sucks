<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 12/28/2014
 * Time: 9:35 AM
 *
 * Gathers the parameters needed to pass into Easy\Controllers
 * Searches each line from given file with the regular expression text
 *
 * The parent File gives the following
 * $min - min number to search by in the kyeword array
 * $max - max number character in the keyword array
 * $keyword - array of keywords to search for in each line
 *
 * @return array $params
 *
 */


$re = "/\\d{1,2}[-\\/.]\\d{1,2}[-\\/.]\\d{1,4}/";
preg_match_all($re, $line, $matches);

if (count($matches[0])) {
  $receipt_date = array();
  for ($i = 0; $i < count($matches[0]); $i++) {
    $dates = explode("/", $matches[0][$i]);
    $newDateFormat = "";
    for ($k = 0; $k < count($dates); $k++) {

      $d = $dates[$k];
      if (strlen($d) > 2) {
        $d = substr($d, 2);
      }
      if ($i === 2) {
        $d = "20$d";
      }
      $newDateFormat .= $d . "/";
    }
    array_push($receipt_date, rtrim($newDateFormat, "/"));
  }
}




