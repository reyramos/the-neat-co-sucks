<?php
  /**
   *
   * Get the file contents, including nested directories in a
   * JSON result
   *
   * Ex:
   * php scan_dir.php [directory path]
   * php getDirContents.php --path="../uploads" --uid 54bdaa68ae715cc22b56a117
   */

  use Easy\Enums\Collection;
  use Easy\Models\MongoConnect;

  require dirname(__DIR__) . '/rachet/vendor/autoload.php';
  require dirname(__DIR__) . "/configs/easy/rachet/constants.php";

  $shortOpts = "p::u:";
  $longOpts = array(
    "path::",     // Directory path optional value
    "uid:",     // Required: user id
  );
  $options = getopt($shortOpts, $longOpts);

//assign
  foreach ($options as $key => $val) $$key = $val;

  $proceed = isset($uid) ? true : (isset($u) ? true : false);

  if (!$proceed)
    throw new Exception('missing options --uid required');

  $uid = isset($uid) ? $uid : $u;

  if (empty($uid))
    throw new Exception('--uid is required');

//lets finally load some stuff
  MongoConnect::configure(CONFIGS . '/environment.php');

  $mongo = new MongoConnect();
  $result = $mongo->MongoFind(array('_id' => new MongoId($uid)), Collection::USERS);

  if (!$result)
    throw new Exception('invalid --uid');

//set the directory of all the files to gather
  $directory = (isset($path) ? UPLOAD_DIR . "/" . $path : (isset($p) ? UPLOAD_DIR . "/" . $p : UPLOAD_DIR)) . "/" . $uid;

  if (empty($directory) || !is_dir($directory))
    throw new Exception('--path is not a directory');

  class recursive_scan extends MongoConnect
  {

    private $directory = "";
    private $current_location = array();


    /**
     * Set Directory to gather the files
     * @param string $directory
     */
    function __construct($directory)
    {
      //add the directory path seperator in case it was not found
      $this->directory = $directory . (strripos($directory, "/") == strlen($directory) - 1 ? "" : "/");
      parent::__construct();


      if (!file_exists($this->directory))
        echo "DIRECTORY DOES NOT EXITS: " . $this->directory;

    }

    function getContent()
    {
      $array = array();
      $dir = empty($this->current_location) ? "" : end($this->current_location);

      if (!empty($this->current_location)) {
        $dir = "";
        foreach ($this->current_location as $loc)
          $dir .= "$loc/";
      }

      $path = $this->directory . $dir;
      $content = scandir($path);
      foreach ($content as $k => $file) {

        //exclude any root or hidden folders
        if ($file != ".." && $file != "." && $file[0] != "."):
          if (is_dir($path . $file)) {

            //push the directory in the array to be tested
            array_push($this->current_location, $file);

            $results = $this->getContent();
            $handler = array();
            foreach ($results as $index => $subFile) {
              if (is_numeric($index)) {
                array_push($handler, $subFile);
              }
            }

            //recursive
            $array["folders"][] = array(
              "name" => $file,
              "_id" => $file,
              "path" => $path,
              "files" => $handler,
              "folders" => array_key_exists("folders", $results) && isset($results["folders"]) ? $results["folders"] : array()
            );
            //delete the folder from array
            array_pop($this->current_location);
          } else {
            $array[] = $this->decorate($path . $file);
          }
        endif;
      }


      return $array;
    }

    private function decorate($filename)
    {

      $pathinfo = pathinfo($filename);

      $fileTypes = array(
        "fa-file-excel-o" => array("xls", "xlsx", "ods", "ots"),
        "fa-file-pdf-o" => array("pdf"),
        "fa-file-word-o" => array("doc", "docx","gdoc"),
        "fa-file-image-o" => array("img", "jpg", "jpeg", "gif", "svg", "png"),
        "fa-file-sound-o" => array("wav", "mp3", "ogg"),
        "fa-film" => array("wav", "mpg", "mp4", "ogg", "psd"),
        "fa-file-text-o" => array("txt", "md", "odt", "ott"),
        "fa-file-code-o" => array("js", "php", "xml", "xmlx", "gsheet"),
        "fa-file-powerpoint-o" => array("point"),
        "fa-file-archive-o" => array("zip", "tar", "rar"),
      );

      $class = "fa-file-o";
      foreach ($fileTypes as $key => $param) {
        if (in_array(strtolower(trim($pathinfo["extension"])), $param)) {
          $class = $key;
          continue;
        }
      }

      $result = false;
      try{
        $result = parent::MongoFind(array('_id' => new \MongoId($pathinfo['filename'])), Collection::FILES);
      }catch (MongoException $e){}



      return array(
        "file" => $filename
      , "ext" => $pathinfo["extension"]
      , "class" => $class
      , "db_data" => $result
      , "basename" => $pathinfo['basename']
      , "dirname" => $pathinfo['dirname']
      , "extension" => $pathinfo['extension']
      , "filename" => $pathinfo['filename']
      , "last_modified" => date("F d Y H:i:s", filemtime($filename))
      , "file_size" => $this->fileSizeConvert(filesize($filename))
      );

    }

    private function fileSizeConvert($bytes)
    {
      $bytes = floatval($bytes);
      $arBytes = array(
        0 => array(
          "UNIT" => "TB",
          "VALUE" => pow(1024, 4)
        ),
        1 => array(
          "UNIT" => "GB",
          "VALUE" => pow(1024, 3)
        ),
        2 => array(
          "UNIT" => "MB",
          "VALUE" => pow(1024, 2)
        ),
        3 => array(
          "UNIT" => "KB",
          "VALUE" => 1024
        ),
        4 => array(
          "UNIT" => "B",
          "VALUE" => 1
        ),
      );

      foreach($arBytes as $arItem)
      {
        if($bytes >= $arItem["VALUE"])
        {
          $result = $bytes / $arItem["VALUE"];
          $result = strval(round($result, 2))." ".$arItem["UNIT"];
          break;
        }
      }
      return $result;
    }


  }


  $a = new recursive_scan($directory);
  $array = $a->getContent();

  $single_files = array();

  foreach ($array as $index => $files) {
    if (is_numeric($index)) {
      array_push($single_files, $files);
    }
  }

  $finalResults = array(
    "files" => $single_files,
    "folders" => $array["folders"] === null ? array() : $array["folders"],
  );

  echo json_encode($finalResults);

