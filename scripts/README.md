#Explode 

The explode file will read every PDF within the path and extract any additional PDF's within the parent PDF file.

```sh
php explode.php --dir "/home/redroger/development/easy/uploads/54bdaa68ae715cc22b56a117/Receipts/2014"
```

The explode.php depends on /rachet/reader.php file which it will first read the number of pages from the PDF file.
For each file, explode.php will execute the following command.

```php
php ../rachet/reader.php --job COUNT --file [FILENAME]
```

It will return an array with the following information:

```php
      $result = array(
        'broadcast' => 'wsFileTransfer',
        'code'      => 200,
        'pages'     => $pageCount,
        'filename'  => $filename,
        'request'   => Status::COUNT,
      );
```

Once we have gather the number of pages, well execute a loop to seperate each individial file from the parent_file 
into single PDF's files.

```php
php ../rachet/reader.php --job EXPLODE --file "[FILENAME]" --page="[PAGE_NUMBER_TO_EXTRACT]"
```

The [PAGE_NUMBER_TO_EXTRACT] are page index to extract from parent_file. ex:1,2,3 ...

#Read

The read script will read every file that was generated from explode. It will gather all the necessary data from the 
REGEX folder and load them into the database. 

```sh
php read.php --dir "/home/redroger/development/easy/uploads/54bdaa68ae715cc22b56a117/Receipts/2014"
```
The read.php depends on /rachet/reader.php file which it will read the contents of the file using tesseract-ocr and 
use regex from REGEX's directory to extract the content onto the database.

```php
php ../rachet/reader.php --job READ --file [FILENAMEE]
```
