<?php
  /**
   * Created by PhpStorm.
   * User: redroger
   * Date: 1/25/2015
   * Time: 6:58 PM
   *
   * php getFile.php --file "//media/volume1/easy/uploads/54bdaa68ae715cc22b56a117/Copyofresume.docx.doc"
   * php getFile.php --file "//media/volume1/easy/uploads/54bdaa68ae715cc22b56a117/4319 - procedure names and
   * parms_files/SPEC 4319 - Add user settings template for IS printing and column display of scheduler.docx"
   *
   */
  use Easy\Enums\MsgCodes;

  require dirname(__DIR__) . '/rachet/vendor/autoload.php';
  require dirname(__DIR__) . "/configs/easy/rachet/constants.php";

  $shortOpts = "f:";
  $longOpts = array(
    "file:",     // required: path value
  );
  $options = getopt($shortOpts, $longOpts);

//assign
  foreach ($options as $key => $val) $$key = $val;

  //set the directory of all the files to gather
  $file = (isset($file) ? $file : (isset($f) ? $f : false));

  if (!$file)
    throw new Exception('missing options --file required');

  if (empty($file) || !is_file($file))
    throw new Exception('--file is not a filepath');


  class fileConversion {

    private $pathinfo;
    private $tmpPath;
    //identifier to proceed to continue
    private $pdftohtml = true; //create the html file of pdf


    function __construct($file)
    {
      $this->pathinfo = pathinfo($file);
      $this->tmpPath = $this->pathinfo['dirname'] . "/.tmp/"; //create a tmp directory in every directory path requested with

      //lets make the .tmp file if not exist
      if (!is_dir($this->tmpPath)) mkdir($this->tmpPath);

      /**********************************************************************
       * Before we continue lets check if the file has already been created,
       * by comparing the md5checksum against itself
       *********************************************************************/
      exec('md5sum -b ' . escapeshellarg($file), $output, $return);
      if (!$return) {
        $md5sum = substr($output[0], 0, strpos($output[0], "*"));
      }

      /**********************************************************************
       * Check if the file exist and compare the string with the md5sum
       *********************************************************************/
      if (file_exists($this->tmpPath . $this->pathinfo['filename'] . ".md5")) {
        $content = file_get_contents($this->tmpPath . $this->pathinfo['filename'] . ".md5");
        if ($content === $md5sum) {
          $this->pdftohtml = false; //create the html file of pdf
        }
      }

      /**********************************************************************
       * If the identifier is true, the file has change or does not exist,
       * proceed to created pdf, html, txt content
       *********************************************************************/
      if ($this->pdftohtml) {
        //save the file md5sum into a file in the temp folder, for later checking
        file_put_contents($this->tmpPath . $this->pathinfo['filename'] . ".md5", $md5sum);

      }
    }

    /**
     * Gets the File Path information,
     * @return array
     */
    function getPathInfo()
    {

      if ($this->pdftohtml) {
        $this->doc2pdf($this->pathinfo['dirname'] . "/" . $this->pathinfo['basename']);
        $this->pdf2html();
        $this->pdf2text();
      } else {
        $this->getPathCopies();
      }

      return $this->pathinfo;
    }

    function getPathCopies()
    {

      $this->countPages();
      $this->getHTMLpages();
      $this->getTXTpages();

    }

    function doc2pdf($file)
    {
      //lets first convert the file to pdf
      exec('doc2pdf  -o "' . $this->tmpPath . $this->pathinfo['filename'] . '.pdf" "' . $file . '"', $output, $return);
      if (!$return) {
        $this->countPages();
      } else {
        $this->pathinfo = array_merge($this->pathinfo, array(
          'code'   => 500,
          'status' => "EASY.CONVERT_PDF:" . MsgCodes::msg_toString(MsgCodes::FAILED_COPY)
        ));
      }
    }

    private function countPages()
    {
      exec('php ' . dirname(__DIR__) . '/rachet/reader.php --job  COUNT  --file "' . $this->tmpPath . $this->pathinfo['filename'] . '.pdf"', $output, $return);
      if (!$return) {
        $this->pathinfo = array_merge($this->pathinfo, array(
          'code'  => 200,
          'pages' => json_decode($output[0], true)['pages'],
          'size' => json_decode($output[0], true)['size'],
        ));
      }
    }

    function pdf2html()
    {
      /**
       * Convert the PDF to HTML
       */
      $pdftohtml = 'pdftohtml -c "' . $this->tmpPath . $this->pathinfo['filename'] . '.pdf"  "' . $this->tmpPath . $this->pathinfo['filename'] . '.html"';

      exec($pdftohtml, $output, $return);
      if (!$return) {

        $this->getHTMLpages();

      } else {
        $this->pathinfo = array_merge($this->pathinfo, array(
          'code'   => 500,
          'status' => "EASY.CONVERT_HTML:" . MsgCodes::msg_toString(MsgCodes::FAILED_COPY)
        ));
      }

    }

    private function getHTMLpages()
    {
      $htmlPages = array();
      $pages = isset($this->pathinfo['pages']) ? $this->pathinfo['pages'] : 1;

      for ($i = 1; $i <= $pages; $i++) {
        $htmlPages[] = $this->tmpPath . $this->pathinfo['filename'] . "-$i.html";
      }

      $this->pathinfo = array_merge($this->pathinfo, array(
        'code'       => 200,
        'html_file'  => $this->tmpPath . $this->pathinfo['filename'] . '.html',
        'pdf_file'   => $this->tmpPath . $this->pathinfo['filename'] . '.pdf',
        'html_pages' => $htmlPages,
        'url' => str_replace("//media/volume1","http://192.168.1.22", $this->tmpPath . $this->pathinfo['filename'] . '.html')
      ));
    }


    /**
     * Create PDF to text
     */
    function pdf2text()
    {
      /**
       * Convert the PDF to TEXT
       */
      $pdftotxt = 'pdftotext  "' . $this->tmpPath . $this->pathinfo['filename'] . '.pdf"  "' . $this->tmpPath .
        $this->pathinfo['filename'] . '.txt"';

      exec($pdftotxt, $output, $return);
      if (!$return) {

        $this->getTXTpages();

      } else {
        $this->pathinfo = array_merge($this->pathinfo, array(
          'code'   => 500,
          'status' => "EASY.CONVERT_HTML:" . MsgCodes::msg_toString(MsgCodes::FAILED_COPY)
        ));
      }
    }

    private function getTXTpages()
    {
      $this->pathinfo = array_merge($this->pathinfo, array(
        'code'     => 200,
        'txt_file' => $this->tmpPath . $this->pathinfo['filename'] . '.txt',
        'pdf_file' => $this->tmpPath . $this->pathinfo['filename'] . '.pdf',
      ));

    }

  }


  $convert = new fileConversion($file);
  $result = $convert->getPathInfo();

  echo json_encode($result);


