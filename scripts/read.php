<?php
  /**
   * Created by PhpStorm.
   * User: redroger
   * Date: 12/26/2014
   * Time: 12:41 AM
   *
   */
  use Easy\Enums\Collection;
  use Easy\Models\MongoConnect;

  require dirname(__DIR__) . '/rachet/vendor/autoload.php';
  require dirname(__DIR__) . "/configs/easy/rachet/constants.php";

  $shortOpts = "d:u::";
  $longOpts = array(
    "dir:",     // Directory path Required value
    "uid::",     // Required: user id
  );
  $options = getopt($shortOpts, $longOpts);

  //assign
  foreach ($options as $key => $val) $$key = $val;

  $proceed = isset($dir) || isset($d) ? true : false;

  if (!$proceed)
    throw new Exception('missing options --dir required, --uid required');


  $dir = isset($dir) ? $dir : $d;
  $uid = isset($uid) ? $uid : (isset($u) ? $u : false);


  if (empty($dir) || !is_dir($dir))
    throw new Exception('--dir is not a directory');

  if (!empty($uid)) {

    //lets finally load some stuff
    MongoConnect::configure(CONFIGS . '/environment.php');


    $mongo = new MongoConnect();
    $result = $mongo->MongoFind(array('_id' => new MongoId($uid)), Collection::USERS);

    if (!$result)
      throw new Exception('invalid --uid');

  }

  $files = scandir($dir);

  class reader {

    private $filename = "";
    private $uid;

    function __construct($filename, $uid)
    {
      $this->uid = $uid;
      $this->filename = $filename;
      echo "LOADING:" . $filename . "\n";
      $this->read();
    }

    function read()
    {
      exec('php ' . dirname(__DIR__) . '/rachet/reader.php --job READ --file "' . $this->filename . '"', $output, $return);
      if ($return) {
        echo "EASY.READ_FILE:ERROR\n";
      }
    }

    /**
     * automagick function,
     * called on destroy of the instance
     */
    public function __destruct()
    {
    }

  }


  foreach ($files as $file) {
    if ($file !== ".." && $file !== "." && !is_dir($dir . "/" . $file)) {
      $filename = $dir . "/" . $file;
      $reader = new reader($filename, $uid);
      $reader = null;

    }
  }
