<?php
  /**
   * Created by PhpStorm.
   * User: redroger
   * Date: 12/23/2014
   * Time: 8:32 PM
   *
   * Scans the directory for Receipts in PDF format and explodes the files into
   * single files
   *
   *
   *
   */


  use Easy\Enums\Collection;
  use Easy\Models\MongoConnect;

  require dirname(__DIR__) . '/rachet/vendor/autoload.php';
  require dirname(__DIR__) . "/configs/easy/rachet/constants.php";

  $shortOpts = "d:u::";
  $longOpts = array(
    "dir:",     // Directory path Required value
    "uid::",     // Required: user id
  );
  $options = getopt($shortOpts, $longOpts);

  //assign
  foreach ($options as $key => $val) $$key = $val;

  $proceed = isset($dir) || isset($d) ? true : false;

  if (!$proceed)
    throw new Exception('missing options --dir required, --uid required');


  $dir = isset($dir) ? $dir : $d;
  $uid = isset($uid) ? $uid : (isset($u) ? $u : false);


  if (empty($dir) || !is_dir($dir))
    throw new Exception('--dir is not a directory');

  if (!empty($uid)) {

    //lets finally load some stuff
    MongoConnect::configure(CONFIGS . '/environment.php');


    $mongo = new MongoConnect();
    $result = $mongo->MongoFind(array('_id' => new MongoId($uid)), Collection::USERS);

    if (!$result)
      throw new Exception('invalid --uid');

  }

  $files = scandir($dir);

  class explode {

    private $basename = "";
    private $filename = "";
    private $index = 1;
    private $uid;

    function __construct($filename, $uid)
    {
      $this->uid = $uid;
      $this->filename = $filename;
      $this->basenam = basename($filename, ".pdf");

      echo "LOADING:" . $filename . "\n";
      $this->read();
    }

    function read()
    {
      $COUNT = json_decode(exec('php ' . dirname(__DIR__) . '/rachet/reader.php --job COUNT --file "' . $this->filename . '"'), true);
      for ($i = 1; $i <= $COUNT['pages']; $i++) {
        $this->index = $i;

        $NEW = json_decode(exec('php ' . dirname(__DIR__) . '/rachet/reader.php --job EXPLODE --file "' .$this->filename . '" --page="' . $this->index . '"'), true);
        if (!empty($NEW['new_file_name']))
          echo "NEW:" . $NEW['new_file_name'] . "\n";
      }


      unlink($this->filename); //remove the uploaded file

    }

    /**
     * automagick function,
     * called on destroy of the instance
     */
    public function __destruct()
    {
    }

  }


  foreach ($files as $file) {
    if ($file !== ".." && $file !== "." && !is_dir($dir . "/" . $file)) {
      $filename = $dir . "/" . $file;

      $reader = new explode($filename, $uid);

      //destroy the object
      $reader = null;

    }
  }


